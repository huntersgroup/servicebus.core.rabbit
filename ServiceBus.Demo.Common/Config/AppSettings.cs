namespace ServiceBus.Demo.Common.Config;

public class AppSettings
{
    public Uri EsbAddress { get; set; }

    public Credentials EsbCredentials { get; set; }
}
