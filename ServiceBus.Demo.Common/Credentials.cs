namespace ServiceBus.Demo.Common;

/// <summary>
/// 
/// </summary>
public class Credentials
{
    /// <summary>
    /// Gets or sets the username.
    /// </summary>
    public required string Username { get; set; }

    /// <summary>
    /// Gets or sets the password.
    /// </summary>
    public required string Password { get; set; }
}
