using Microsoft.Extensions.Configuration;

namespace ServiceBus.Demo.Common.Extensions;

public static class CommonExtensions
{
    internal static string GetExecutionEnv(this object app)
    {
        return Environment.GetEnvironmentVariable("NETCOREAPP_ENVIRONMENT");
    }

    public static IConfigurationRoot BuildConfiguration(this object app)
    {
        var env = app.GetExecutionEnv();

        var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env}.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables();

        var configuration = builder.Build();

        return configuration;
    }
}
