using System;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Formatters;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Subscribers;

public class SubscriberTest
{
    private readonly IDataDecoder decoder;

    public SubscriberTest()
    {
        this.decoder = new JsonDataFormatter(new JsonSerializerSettings(), Encoding.UTF8);
    }
        
    [Fact]
    public void WrongCommonSubscriberTest()
    {
        // arrange
        var serviceName = Guid.NewGuid().ToString("N");
        var descriptor = new SubscriberDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = serviceName }
        };

        // test
        Assert.Throws<ArgumentNullException>(() => new CommonSubscriber<string>(null, null));
        Assert.Throws<ArgumentNullException>(() => new CommonSubscriber<string>(null, this.decoder));
        Assert.Throws<ArgumentNullException>(() => new CommonSubscriber<string>(descriptor, null));
    }

    [Fact]
    public void WrongBinderSubscriberTest()
    {
        // arrange
        var serviceName = Guid.NewGuid().ToString("N");
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = serviceName, AutoDelete = true },
            Exchange = { Name = serviceName, AutoDelete = true }
        };

        // asserts
        Assert.Throws<ArgumentNullException>(() => new BinderSubscriber<string>(null, null));
        Assert.Throws<ArgumentNullException>(() => new BinderSubscriber<string>(null, this.decoder));
        Assert.Throws<ArgumentNullException>(() => new BinderSubscriber<string>(descriptor, null));
    }
}
