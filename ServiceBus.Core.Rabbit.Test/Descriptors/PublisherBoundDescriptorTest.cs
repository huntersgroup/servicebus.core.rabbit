using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Model;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class PublisherBoundDescriptorTest
{
    public PublisherBoundDescriptorTest()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
            .AddEnvironmentVariables();

        this.Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    [Fact]
    public void OnCreationTest()
    {
        var desc = new PublisherBoundDescriptor();

        // asserts
        Assert.False(desc.EnableChannelEvents);
        Assert.NotNull(desc.Message);
        Assert.NotNull(desc.Confirmation);
        Assert.NotNull(desc.Exchanges);
        Assert.NotNull(desc.OnBuildProperties);
    }

    [Fact]
    public void OnLoadFromConfigDefault()
    {
        var desc = this.Configuration.Get<AppSettings>()
            .DefaultPublisherBound;

        // asserts
        Assert.False(desc.EnableChannelEvents);
        Assert.NotNull(desc.Message);
        Assert.NotNull(desc.Confirmation);
        Assert.NotNull(desc.Exchanges);
        Assert.NotNull(desc.OnBuildProperties);

        desc.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusNoExchangeTest()
    {
        var desc = new PublisherBoundDescriptor
        {
            Exchanges = null
        };

        // assert: exchange null reference
        Assert.Throws<InvalidDescriptorException>(() => desc.VerifyStatus());

        // act: empty exchanges collection 
        desc.Exchanges = new List<ExchangeBoundDescriptor>();
        Assert.Throws<InvalidDescriptorException>(() => desc.VerifyStatus());

        // acts
        desc.Exchanges.Add(new ExchangeBoundDescriptor
        {
            Name = "demo", Type = ExchangeTypes.Headers
        });

        // assert: no exception on calling
        desc.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusWithDuplicateExchangesTest()
    {
        var desc = new PublisherBoundDescriptor
        {
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "ex-01", Type = ExchangeTypes.Headers
                },
                new ExchangeBoundDescriptor
                {
                    Name = "ex-01", Type = ExchangeTypes.Direct
                }
            }
        };

        // asserts
        Assert.Throws<DuplicateDescriptorException>(() => desc.VerifyStatus());
    }

    [Fact]
    public void VerifyStatusWithInnerDuplicateExchangesTest()
    {
        var desc = new PublisherBoundDescriptor
        {
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "ex-01", Type = ExchangeTypes.Fanout
                },
                new ExchangeBoundDescriptor
                {
                    Name = "ex-02", Type = ExchangeTypes.Direct,
                    Exchanges =
                    {
                        new ExchangeBoundDescriptor{ Name = "ex-01-01", Type = ExchangeTypes.Topic },
                        new ExchangeBoundDescriptor
                        {
                            Name = "ex-01-02", Type = ExchangeTypes.Topic ,
                            Exchanges =
                            {
                                new ExchangeBoundDescriptor{ Name = "ex-01-01-01" },
                                new ExchangeBoundDescriptor{ Name = "ex-01-01-01" }
                            }
                        }
                    }
                }
            }
        };

        // asserts
        Assert.Throws<DuplicateDescriptorException>(() => desc.VerifyStatus());
    }

    [Fact]
    public void VerifyStatusWithInnerDuplicateQueuesTest()
    {
        var desc = new PublisherBoundDescriptor
        {
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "ex-01", Type = ExchangeTypes.Headers,
                    Queues =
                    {
                        new QueueBoundDescriptor{ Name = "queue-01", Durable = true },
                        new QueueBoundDescriptor{ Name = "queue-02", Durable = true, Exclusive = true }
                    },
                    Exchanges =
                    {
                        new ExchangeBoundDescriptor{ Name = "exchange-01", Durable = true, Type = ExchangeTypes.Direct },
                        new ExchangeBoundDescriptor
                        {
                            Name = "exchange-02", Prefix = "esb/xx", Durable = true, Type = ExchangeTypes.Direct,
                            Queues =
                            {
                                new QueueBoundDescriptor{ Name = "queue-xx-01", Durable = true },
                                new QueueBoundDescriptor{ Name = "queue-xx-01", Durable = true, Exclusive = true }
                            }
                        },
                    }
                }
            }
        };

        // asserts
        Assert.Throws<DuplicateDescriptorException>(() => desc.VerifyStatus());
    }

    [Fact]
    public void VerifyStatusTest()
    {
        var desc = new PublisherBoundDescriptor
        {
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "ex-01", Type = ExchangeTypes.Headers,
                    Queues =
                    {
                        new QueueBoundDescriptor{ Name = "queue-01", Durable = true },
                        new QueueBoundDescriptor{ Name = "queue-01", Prefix = "qq", Durable = true, Exclusive = true }
                    },
                    Exchanges =
                    {
                        new ExchangeBoundDescriptor{ Name = "exchange-01", Durable = true, Type = ExchangeTypes.Direct },
                        new ExchangeBoundDescriptor{ Name = "exchange-01", Prefix = "esb/xx", Durable = true, Type = ExchangeTypes.Direct }
                    }
                }
            }
        };

        desc.VerifyStatus();
    }
}
