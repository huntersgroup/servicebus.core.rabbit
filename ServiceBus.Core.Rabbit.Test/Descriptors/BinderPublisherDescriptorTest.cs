using System;
using Microsoft.Extensions.Configuration;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class BinderPublisherDescriptorTest
{
    public BinderPublisherDescriptorTest()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
            .AddEnvironmentVariables();

        this.Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    [Fact]
    public void VerifyStatusTest()
    {
        // arrange
        var descriptor = new BinderPublisherDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = Guid.NewGuid().ToString("N") },
            Exchange = { Name = Guid.NewGuid().ToString("N") }
        };

        // assert (no exception is thrown!)
        descriptor.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusWithExceptionsTest()
    {
        // arrange
        var rightDescriptor = new BinderPublisherDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = Guid.NewGuid().ToString("N") },
            Exchange = { Name = Guid.NewGuid().ToString("N") }
        };

        var badDescriptor = new BinderPublisherDescriptor
        {
            Address = rightDescriptor.Address,
            Credentials = rightDescriptor.Credentials,
            Exchange = rightDescriptor.Exchange
        };

        // asserts (misses Queue property)
        Assert.Throws<InvalidDescriptorException>(() => badDescriptor.VerifyStatus());

        // acts
        badDescriptor.Queue = rightDescriptor.Queue;

        // assert (no exception)
        badDescriptor.VerifyStatus();

        // acts
        badDescriptor.Exchange = null;

        // asserts (misses Exchange property)
        Assert.Throws<InvalidDescriptorException>(() => badDescriptor.VerifyStatus());
    }

    [Fact]
    public void LoadFromSettings()
    {
        var appSettings = this.Configuration.Get<AppSettings>();

        Assert.NotNull(appSettings.BinderPublisherDescriptor);
    }
}
