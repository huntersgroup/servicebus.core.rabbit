using System;
using Microsoft.Extensions.Configuration;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class BrokerDescriptorTest
{
    private readonly ITestOutputHelper output;

    public BrokerDescriptorTest(ITestOutputHelper output)
    {
        this.output = output;

        var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
            .AddEnvironmentVariables();

        this.Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    [Fact]
    public void VerifyStatusTest()
    {
        var descriptor = new BrokerDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" }
        };

        // assert (no exception is thrown!)
        descriptor.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusWithExceptionsTest()
    {
        // arrange
        var rightDescriptor = new BrokerDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" }
        };

        var descriptor = new BrokerDescriptor
        {
            Credentials = rightDescriptor.Credentials
        };

        // asserts
        Assert.Throws<InvalidDescriptorException>(() => descriptor.VerifyStatus());

        // arrange
        descriptor.Address = rightDescriptor.Address;
        descriptor.Credentials = null;

        // asserts
        Assert.Throws<InvalidDescriptorException>(() => descriptor.VerifyStatus());
    }

    [Fact]
    public void LoadFromSettings()
    {
        var appSettings = this.Configuration.Get<AppSettings>();

        // asserts (only for deserialization)
        Assert.NotNull(appSettings.BrokerDescriptor);
    }
}
