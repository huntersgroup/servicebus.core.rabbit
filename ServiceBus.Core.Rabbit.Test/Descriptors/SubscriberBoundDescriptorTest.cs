using Microsoft.Extensions.Configuration;
using System;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Exceptions;
using Xunit;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class SubscriberBoundDescriptorTest
{
    public SubscriberBoundDescriptorTest()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
            .AddEnvironmentVariables();

        this.Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    [Fact]
    public void OnCreationTest()
    {
        var desc = new SubscriberBoundDescriptor();

        // asserts
        Assert.False(desc.EnableChannelEvents);
        Assert.Equal(MessageFormat.Header, desc.MessageFormat);
        Assert.Equal(1, desc.PrefetchCount);
        Assert.NotNull(desc.RetryPolicy);
        Assert.NotNull(desc.Arguments);
        Assert.NotNull(desc.Queue);
        Assert.Null(desc.Binder);
        Assert.Null(desc.Exchange);
    }

    [Fact]
    public void VerifyStatusNoQueueTest()
    {
        var desc = new SubscriberBoundDescriptor
        {
            Queue = null
        };

        // assert: queue null reference
        Assert.Throws<InvalidDescriptorException>(() => desc.VerifyStatus());

        desc.Queue = new QueueDescriptor();

        // assert: it needs to be setup
        Assert.Throws<InvalidDescriptorException>(() => desc.VerifyStatus());

        desc.Queue = new QueueDescriptor
        {
            Name = "ciccio"
        };

        desc.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusInvalidExchangeTest()
    {
        var desc = new SubscriberBoundDescriptor
        {
            Queue = new QueueDescriptor
            {
                Name = "ciccio"
            },
            Exchange = new ExchangeDescriptor()
        };

        // assert: exchange invalid
        Assert.Throws<InvalidDescriptorException>(() => desc.VerifyStatus());

        // acts
        desc.Exchange.Name = "Test";

        desc.VerifyStatus();
    }
}
