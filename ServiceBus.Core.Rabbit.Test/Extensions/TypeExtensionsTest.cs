using System;
using ServiceBus.Core.Extensions;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Extensions;

public class TypeExtensionsTest
{
    [Theory]
    [InlineData(typeof(Message<string>), typeof(IMessage))]
    [InlineData(typeof(Message<string>), typeof(IMessage<string>))]
    [InlineData(typeof(Message<string>), typeof(IMessage<>))]
    [InlineData(typeof(IMessage<>), typeof(IMessage<>))]
    [InlineData(typeof(IMessage<string>), typeof(IMessage<>))]
    [InlineData(typeof(IMessage<>), typeof(IMessage))]
    public void Implements_Test(Type current, Type interfaceType)
    {
        Assert.True(current.Implements(interfaceType));
    }
}
