using System.Collections.Generic;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Rabbit.Test;

public class AppSettings
{
    public IDictionary<string, object> Arguments { get; set; }

    public BrokerDescriptor BrokerDescriptor { get; set; }

    public BinderPublisherDescriptor BinderPublisherDescriptor { get; set; }

    public BinderPublisherDescriptor MultiBinderPublisherDescriptor { get; set; }

    public PublisherBoundDescriptor DefaultPublisherBound { get; set; }
}
