using System;
using System.Text;
using Newtonsoft.Json;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Publishers;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Formatters;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Publishers;

public class PublisherTest
{
    private readonly IDataEncoder encoder;

    public PublisherTest()
    {
        this.encoder = new JsonDataFormatter(new JsonSerializerSettings(), Encoding.UTF8);
    }

    [Fact]
    public void WrongCommonPublisherTest()
    {
        // arrange
        var serviceName = Guid.NewGuid().ToString("N");
        var descriptor = new PublisherDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Exchange = { Name = serviceName }
        };

        // test
        Assert.Throws<ArgumentNullException>(() => new CommonPublisher(null, null));
        Assert.Throws<ArgumentNullException>(() => new CommonPublisher(null, this.encoder));
        Assert.Throws<ArgumentNullException>(() => new CommonPublisher(descriptor, null));
    }

    [Fact]
    public void WrongBinderPublisherTest()
    {
        // arrange
        var serviceName = Guid.NewGuid().ToString("N");
        var descriptor = new BinderPublisherDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Exchange = { Name = serviceName },
            Queue = { Name = serviceName }
        };

        // test
        Assert.Throws<ArgumentNullException>(() => new BinderPublisher(null, null));
        Assert.Throws<ArgumentNullException>(() => new BinderPublisher(null, this.encoder));
        Assert.Throws<ArgumentNullException>(() => new BinderPublisher(descriptor, null));
    }
}
