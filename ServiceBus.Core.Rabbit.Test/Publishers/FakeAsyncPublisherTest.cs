using System.Threading.Tasks;
using ServiceBus.Core.Clients.Publishers;
using ServiceBus.Core.Events;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Publishers;

public class FakeAsyncPublisherTest : IAsyncPublisher
{
    public IDelegateHandler<ChannelEventArgs> OnChannelEventFired { get; set; }

    public void Send(object data)
    {
        //if (data.GetType().Implements(typeof(IMessage<>)))
        //{
        //    this.Send<dynamic>(data as dynamic);
        //    //this.Send(data as dynamic);
        //    //this.Send(data as IMessage<dynamic>);
        //    return;
        //}

        if (data is IMessage<dynamic> msg)
        {
            this.Send(msg);
            return;
        }

        this.LastMsgWrapped = true;
    }

    public void Send<TData>(IMessage<TData> message)
    {
        this.LastMsgWrapped = false;
    }

    public Task SendAsync(object data)
    {
        return Task.Factory.StartNew(() => this.Send(data));
    }

    public Task SendAsync<TData>(IMessage<TData> message)
    {
        return Task.Factory.StartNew(() => this.Send(message));
    }

    public bool LastMsgWrapped { get; private set; }

    [Fact]
    public async Task Send_Test()
    {
        var publisher = new FakeAsyncPublisherTest();
        var message = new MessageV2<string>
        {
            Data = "hello..",
            Counter = 1
        };
        IMessage msg1 = message;
        object msg2 = message;

        //// acts
        //publisher.Send(message);
        //// asserts
        //Assert.False(publisher.LastMsgWrapped);

        // acts
        publisher.Send(msg1);
        // asserts
        Assert.False(publisher.LastMsgWrapped);

        // acts
        publisher.Send(msg2);
        // asserts
        Assert.False(publisher.LastMsgWrapped);

        // acts
        await publisher.SendAsync(msg1);
        // asserts
        Assert.False(publisher.LastMsgWrapped);

        // acts
        await publisher.SendAsync(msg2);
        // asserts
        Assert.False(publisher.LastMsgWrapped);
    }

    [Fact]
    public void Send_WrappedTest()
    {
        var publisher = new FakeAsyncPublisherTest();
        var message = new { Name = "name 1", Surname = "Surname" };
        object msg1 = message;

        // acts
        publisher.Send(message);
        // asserts
        Assert.True(publisher.LastMsgWrapped);

        // acts
        publisher.Send(msg1);
        // asserts
        Assert.True(publisher.LastMsgWrapped);
    }
}
