using ServiceBus.Core.Clients;
using ServiceBus.Core.Events;
using System;
using ServiceBus.Core.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace ServiceBus.Core.Rabbit.Test.Trigger;

public class EventHandlerDecoratorTest
{
    private readonly ITestOutputHelper output;

    public EventHandlerDecoratorTest(ITestOutputHelper output)
    {
        this.output = output;
    }

    private event EventHandler<MessageDeliverEventArgs<string>> Demo;

    [Fact]
    public void TestOnEvent()
    {
        this.Demo = TriggerOnReceived1;
        this.Demo += TriggerOnReceived1;

        Assert.Equal(2, this.Demo.Count());

        this.Demo += null;

        Assert.Equal(2, this.Demo.Count());

        this.Demo -= null;

        Assert.Equal(2, this.Demo.Count());

        this.Demo += TriggerOnReceived2;

        Assert.Equal(3, this.Demo.Count());

        this.Demo = null;

        Assert.Equal(0, this.Demo.Count());

        this.Demo += TriggerOnReceived1;

        Assert.Equal(1, this.Demo.Count());

        this.Demo += null;

        Assert.Equal(1, this.Demo.Count());
    }

    [Fact]
    public void InvokeTest()
    {
        var handler = new EventHandlerDecorator<MessageDeliverEventArgs<string>>();

        // asserts
        Assert.Equal(0, handler.EventHandler.Count());

        // acts
        handler.OnEventRaised += this.TriggerOnReceived1;
        handler.OnEventRaised += this.TriggerOnReceived2;

        // asserts
        Assert.Equal(2, handler.EventHandler.Count());

        var ev = new MessageDeliverEventArgs<string>
        {
            Requeue = false,
            Message = new Message<string>
            {
                Id = "dfsdakfjnjk",
                Data = "ciccio-pasticcio"
            }
        };

        var trigger = handler.EventHandler;
        trigger.Invoke(this, ev);
    }

    [Fact]
    public void CountTest()
    {
        var handler = new EventHandlerDecorator<MessageDeliverEventArgs<string>>();

        // asserts
        Assert.NotNull(handler);
        Assert.Equal(0, handler.EventHandler.Count());

        handler.OnEventRaised += this.TriggerOnReceived1;
        handler.OnEventRaised += this.TriggerOnReceived2;
        handler.OnEventRaised += this.TriggerOnReceived3;
        handler.OnEventRaised += this.TriggerOnReceived1;
            
        Assert.Equal(4, handler.EventHandler.Count());

        handler.OnEventRaised -= this.TriggerOnReceived1;
        Assert.Equal(3, handler.EventHandler.Count());

        handler.OnEventRaised -= this.TriggerOnReceived1;
        Assert.Equal(2, handler.EventHandler.Count());

        handler.OnEventRaised -= this.TriggerOnReceived1;
        Assert.Equal(2, handler.EventHandler.Count());

        handler.OnEventRaised -= this.TriggerOnReceived2;
        Assert.Equal(1, handler.EventHandler.Count());

        handler.OnEventRaised -= this.TriggerOnReceived2;
        Assert.Equal(1, handler.EventHandler.Count());

        handler.OnEventRaised -= this.TriggerOnReceived3;
        Assert.Equal(0, handler.EventHandler.Count());
    }

    [Fact]
    public void ClearTest()
    {
        var handler = new EventHandlerDecorator<MessageDeliverEventArgs<string>>();

        // asserts
        Assert.NotNull(handler);
        Assert.Equal(0, handler.EventHandler.Count());

        handler.OnEventRaised += this.TriggerOnReceived1;
        handler.OnEventRaised += this.TriggerOnReceived2;
        handler.OnEventRaised += this.TriggerOnReceived3;
        handler.OnEventRaised += this.TriggerOnReceived1;

        Assert.Equal(4, handler.EventHandler.Count());

        handler.Clear();

        Assert.Equal(0, handler.EventHandler.Count());
    }

    [Fact]
    public void InvokeWithAfterTest()
    {
        var handler = new EventHandlerDecorator<MessageDeliverEventArgs<string>>();

        // asserts
        Assert.NotNull(handler);
        Assert.Equal(0, handler.EventHandler.Count());

        handler.OnEventRaised += this.TriggerOnReceived1;
        handler.OnEventRaised += this.TriggerOnReceived2;
        handler.OnEventRaised += this.TriggerOnReceived3;

        handler.OnAfterEventRaised += this.TriggerOnAfter1;
        handler.OnAfterEventRaised += this.TriggerOnAfter2;

        Assert.Equal(3, handler.EventHandler.Count());

        var ev = new MessageDeliverEventArgs<string>
        {
            Requeue = false,
            Message = new Message<string>
            {
                Id = "dfsdakfjnjk",
                Data = "ciccio"
            }
        };

        var trigger = handler.EventHandler;
        trigger.Invoke(this, ev);
    }

    [Fact]
    public void InvokeAfterFakeTest()
    {
        var handler = new EventHandlerDecorator<MessageDeliverEventArgs<string>>();

        // asserts
        Assert.NotNull(handler);
        Assert.Equal(0, handler.EventHandler.Count());

        handler.OnEventRaised += TriggerOnReceived1;
        handler.OnEventRaised += TriggerOnReceived2;
        handler.OnEventRaised += TriggerOnReceived3;

        handler.OnAfterEventRaised += TriggerOnAfter1;
        handler.OnAfterEventRaised += TriggerOnAfter2;

        Assert.Equal(3, handler.EventHandler.Count());

        handler.Clear();

        Assert.Equal(0, handler.EventHandler.Count());

        var ev = new MessageDeliverEventArgs<string>
        {
            Requeue = false,
            Message = new Message<string>
            {
                Id = "dfsdakfjnjk",
                Data = "ciccio"
            }
        };

        var trigger = handler.EventHandler;
        trigger?.Invoke(this, ev);
    }

    private void TriggerOnReceived1(object sender, MessageDeliverEventArgs<string> e)
    {
        this.output.WriteLine($"method: {nameof(this.TriggerOnReceived1)}, data: {e.Message.Data}");
    }

    private void TriggerOnReceived2(object sender, MessageDeliverEventArgs<string> e)
    {
        this.output.WriteLine($"method: {nameof(this.TriggerOnReceived2)}, data: {e.Message.Data}");
    }

    private void TriggerOnReceived3(object sender, MessageDeliverEventArgs<string> e)
    {
        this.output.WriteLine($"method: {nameof(this.TriggerOnReceived3)}, data: {e.Message.Data}");
    }

    private void TriggerOnAfter1(object sender, MessageDeliverEventArgs<string> e)
    {
        this.output.WriteLine($"method: {nameof(this.TriggerOnAfter1)}, data: {e.Message.Data}");
    }

    private void TriggerOnAfter2(object sender, MessageDeliverEventArgs<string> e)
    {
        this.output.WriteLine($"method: {nameof(this.TriggerOnAfter2)}, data: {e.Message.Data}");
    }
}
