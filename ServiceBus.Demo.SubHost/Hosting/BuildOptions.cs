namespace ServiceBus.Demo.SubHost.Hosting;

internal record BuildOptions(bool InjectServiceProvider, bool Validate)
{
    //public bool InjectServiceProvider { get; set; } = InjectServiceProvider;
}
