using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ServiceBus.Core.Clients.Subscribers;

namespace ServiceBus.Demo.SubHost.Hosting;

internal class AppWorkerService : BackgroundService
{
    private readonly IHostApplicationLifetime appLifetime;
    private readonly IAsyncRunnable runnable;
    private readonly ILogger logger;
    private readonly string runnableTarget;

    public AppWorkerService(IHostApplicationLifetime appLifetime, IAsyncRunnable runnable, ILogger<AppWorkerService> logger)
    {
        this.appLifetime = appLifetime;
        this.runnable = runnable;
        this.logger = logger;
        this.runnableTarget = runnable.GetType().Name;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        this.appLifetime.ApplicationStarted.Register(async () => await this.OnStarted());
        this.appLifetime.ApplicationStopping.Register(async () => await this.OnStopping());
        this.appLifetime.ApplicationStopped.Register(async () => await this.OnStopped());

        await Task.CompletedTask;
    }

    private async ValueTask OnStarted()
    {
        this.logger.LogInformation($"######## on started at: {DateTime.UtcNow:u}");
            
        try
        {
            await this.runnable.Start();
        }
        catch (Exception e)
        {
            this.appLifetime.StopApplication();
        }
            
        this.logger.LogInformation($"######## {this.runnableTarget} started ########");
        
        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopping()
    {
        this.logger.LogInformation($"######## on stopping at: {DateTime.UtcNow:u}");

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopped()
    {
        this.logger.LogInformation($"######## on stopped at: {DateTime.UtcNow:u}");

        await this.runnable.Stop();

        this.logger.LogInformation($"######## {this.runnableTarget} stopped ########");

        await ValueTask.CompletedTask;
    }
}
