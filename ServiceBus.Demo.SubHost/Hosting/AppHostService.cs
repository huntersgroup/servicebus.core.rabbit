using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace ServiceBus.Demo.SubHost.Hosting;

internal class AppHostService : IHostedService
{
    private readonly IHostApplicationLifetime appLifetime;

    public AppHostService(IHostApplicationLifetime appLifetime)
    {
        this.appLifetime = appLifetime;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        this.appLifetime.ApplicationStarted.Register(async () => await this.OnStarted());
        this.appLifetime.ApplicationStopping.Register(async () => await this.OnStopping());
        this.appLifetime.ApplicationStopped.Register(async () => await this.OnStopped());

        await Task.CompletedTask;
    }
        
    public async Task StopAsync(CancellationToken cancellationToken)
    {
        Console.WriteLine($"$$$$$$44 stopAsync at: {DateTime.UtcNow:u}, this is a stop method execution ....");
        await Task.Delay(TimeSpan.FromSeconds(5), cancellationToken);

        await Task.CompletedTask;
    }

    private async Task OnStarted()
    {
        Console.WriteLine($"######## on started at: {DateTime.UtcNow:u}");
        //Thread.Sleep(TimeSpan.FromSeconds(10));
        await Task.Delay(TimeSpan.FromSeconds(10));

        Console.WriteLine($"######## on started method ended: {DateTime.UtcNow:u}");

        try
        {
            throw new ApplicationException("A simple exception for demo...");
        }
        catch (Exception e)
        {
            this.appLifetime.StopApplication();
        }

        await Task.CompletedTask;
    }

    private async Task OnStopping()
    {
        Console.WriteLine($"######## on stopping at: {DateTime.UtcNow:u}");

        await ValueTask.CompletedTask;

        throw new ApplicationException("######### Exception on Stopping #########");
    }

    private async Task OnStopped()
    {
        Console.WriteLine($"######## on stopped at: {DateTime.UtcNow:u}");

        await ValueTask.CompletedTask;
    }
}
