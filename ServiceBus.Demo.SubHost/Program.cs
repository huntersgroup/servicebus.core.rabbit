using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceBus.Demo.SubHost.Config;
using ServiceBus.Demo.SubHost.Hosting;

namespace ServiceBus.Demo.SubHost;

internal class Program
{
    private static async Task Main(string[] args)
    {
        if (!TryGetFuncSubscriberDescriptor(out var funcSubDescriptor))
        {
            return;
        }

        Console.WriteLine();
        
        var env = Environment.GetEnvironmentVariable("APP_ENV") ?? "dev";

        var hostBuilder = Host.CreateDefaultBuilder(args)
            .ConfigureServiceProvider()
            .ConfigureHostConfiguration(builder =>
            {
            })
            .ConfigureAppConfiguration((context, builder) =>
            {
            })
            .ConfigureServices((context, services) =>
            {
                context.Properties.Add(IocEngineConfig.DescriptorLabel, funcSubDescriptor);

                services.AddSingleton((factory) => new StringBuilder())
                    .RegisterServices(context);
            })
            
            .UseEnvironment(env)
            
            .UseConsoleLifetime();

        await hostBuilder
            .Build(new BuildOptions(true, true))
            .RunAsync();
    }

    /// <summary>
    /// Tries to prepare the function which gets the Descriptor object
    /// </summary>
    /// <param name="funcSubDescriptor"></param>
    /// <returns></returns>
    private static bool TryGetFuncSubscriberDescriptor(out Func<RabbitDescriptors, object> funcSubDescriptor)
    {
        Console.WriteLine("### Choose the subscriber way to execute: ###");
        Console.WriteLine();
        Console.WriteLine($"Press 1 for {nameof(AppSettings.Descriptors.RoutingBridgeBound)}");
        Console.WriteLine($"Press 2 for {nameof(AppSettings.Descriptors.SubscriberBound01)}");

        var method = Console.ReadLine();

        switch (method)
        {
            case "1":
            {
                funcSubDescriptor = descriptors => descriptors.RoutingBridgeBound;
                return true;
            }
            case "2":
            {
                funcSubDescriptor = descriptors => descriptors.SubscriberBound01;
                return true;
            }
            default:
            {
                Console.WriteLine("No option available !!!");
                funcSubDescriptor = null;
                return false;
            }
        }
    }
}
