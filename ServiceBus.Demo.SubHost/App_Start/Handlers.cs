using ServiceBus.Core.Events;
using System.Threading.Tasks;
using System;
using ServiceBus.Demo.Model;

namespace ServiceBus.Demo.SubHost;

internal static class Handlers
{
    internal static async Task BoundOnReceived1Async(object sender, MessageDeliverEventArgs<ComplexCls> args)
    {
        Console.WriteLine($"### started at: {DateTime.Now:u}, created: {args.Message.Created:u}");
        Console.WriteLine($"### job running now ###");
        await Task.Delay(TimeSpan.FromMilliseconds(50));
        Console.WriteLine($"### over at:    {DateTime.Now:u}, task is over ###");
        Console.WriteLine();
    }

    internal static async Task BoundOnReceived1Async(object sender, MessageDeliverEventArgs<ComplexCls> args, Func<object, string> serializerFunc)
    {
        Console.WriteLine($"### started at: {DateTime.Now:u}, created: {args.Message.Created:u}");
        Console.WriteLine($"### job running now ###");
        Console.WriteLine($"### message received: {serializerFunc(args.Message.Data)}");
        await Task.Delay(TimeSpan.FromMilliseconds(50));
        Console.WriteLine($"### over at:    {DateTime.Now:u}, task is over ###");
        Console.WriteLine();
    }

    internal static void BoundOnOnException(object sender, MessageExceptionEventArgs e)
    {
        Console.WriteLine($"An exception was caught, payloadType: {e.PayloadType}, exception: [message: {e.Exception.Message}, stackTrace: {e.Exception.StackTrace}]");
    }
}
