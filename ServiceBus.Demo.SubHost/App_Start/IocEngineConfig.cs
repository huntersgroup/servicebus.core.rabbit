using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.Ioc.SimpleInjector;
using SimpleInjector.Lifestyles;
using System.Reflection;
using Microsoft.Extensions.Hosting;
using ServiceBus.Demo.SubHost.Config;
using SimpleInjector;
using ServiceBus.Demo.SubHost.Hosting;
using Microsoft.Extensions.Configuration;
using ServiceResolver.Ioc;
using System;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Demo.Model;

namespace ServiceBus.Demo.SubHost;

internal static class IocEngineConfig
{
    internal const string ContainerLabel = "_container_";
    internal const string ServiceProviderLabel = "_service-provider_";
    internal const string DescriptorLabel = "_descriptor_";

    internal static RunnableSettings RunnableSettings { get; private set; }

    /// <summary>
    /// Begins to build the service provider.
    /// </summary>
    /// <param name="hostBuilder"></param>
    /// <returns></returns>
    internal static IHostBuilder ConfigureServiceProvider(this IHostBuilder hostBuilder)
    {
        var serviceProvider = new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            ScopeFactory = AsyncScopedLifestyle.BeginScope,
            Initializer = container =>
            {
                container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
                container.Options.DefaultLifestyle = Lifestyle.Singleton;
                hostBuilder.Properties.Add(ContainerLabel, container);
            }
        });
            
        hostBuilder.Properties.Add(ServiceProviderLabel, serviceProvider);
            
        return hostBuilder;
    }

    /// <summary>
    /// Initializes the current host, and it applies the internal service provider.
    /// </summary>
    /// <param name="hostBuilder"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    internal static IHost Build(this IHostBuilder hostBuilder, BuildOptions options)
    {
        var host = hostBuilder.Build();

        if (options.InjectServiceProvider && hostBuilder.Properties[ContainerLabel] is Container container)
        {
            host.UseSimpleInjector(container);
        }

        if (options.Validate && hostBuilder.Properties[ServiceProviderLabel] is ServiceRegisterProvider serviceRegister)
        {
            serviceRegister.Verify();
        }

        return host;
    }

    /// <summary>
    /// Register all services into the current service provider.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    internal static IServiceCollection RegisterServices(this IServiceCollection services, HostBuilderContext context)
    {
        if (context.Properties[ContainerLabel] is Container container)
        {
            services.AddSimpleInjector(container, options =>
            {
                options.AutoCrossWireFrameworkComponents = true;
                options.AddHostedService<AppWorkerService>();
            });
        }

        var serviceProvider = context.Properties[ServiceProviderLabel] as ServiceRegisterProvider;
        var appSettings = context.Configuration.Get<AppSettings>();

        // initialize
        RunnableSettings = GetRunnableSettings(appSettings,
            context.Properties[DescriptorLabel] as Func<RabbitDescriptors, object>);

        serviceProvider?.Register(() => appSettings, LifetimeScope.Singleton);
        serviceProvider?.RegisterModulesFrom(Assembly.GetExecutingAssembly());

        return services;
    }

    private static RunnableSettings GetRunnableSettings(AppSettings settings, Func<RabbitDescriptors, object> func)
    {
        var value = func.Invoke(settings.Descriptors);

        var serviceType = value switch
        {
            RoutingBridgeDescriptor => typeof(RoutingBridgeBound<ComplexCls>),
            SubscriberBoundDescriptor => typeof(SubscriberBound<ComplexCls>),
            _ => throw new ArgumentException("No deterministic runnable type.")
        };

        return new RunnableSettings
        {
            ServiceType = serviceType,
            Descriptor = value
        };
    }
}
