using System.Text.Json.Serialization;
using System.Text.Json;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Formatters.Extensions;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;

namespace ServiceBus.Demo.SubHost.Modules;

//[Obsolete]
//[Description("Obsolete only not to load in DI.")]
internal class MsDataFormatterModule : IServiceModule
{
    public void Initialize(IServiceRegister serviceRegister)
    {
        serviceRegister.Register<IDataFormatter, MsJsonDataFormatter>()
            .Register(provider =>
        {
            var options = new JsonSerializerOptions
            {
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                ReferenceHandler = ReferenceHandler.IgnoreCycles,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
            };

            options.Converters.Add(new JsonStringEnumConverter());

            options.ApplyRealNumberWithFractionalPortion();

            return options;
        });
    }
}
