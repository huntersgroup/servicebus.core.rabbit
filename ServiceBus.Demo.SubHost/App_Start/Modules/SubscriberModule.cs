using System;
using System.Text.Json;
using RabbitMQ.Client;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Demo.Model;
using ServiceBus.Demo.SubHost.Config;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;

namespace ServiceBus.Demo.SubHost.Modules;

internal class SubscriberModule : IServiceModule
{
    public void Initialize(IServiceRegister serviceRegister)
    {
        // connection factory
        serviceRegister.Register<IConnectionFactory>(provider =>
        {
            var settings = provider.GetService<AppSettings>();

            return new ConnectionFactory
            {
                Uri = settings.EsbAddress,
                UserName = settings.EsbCredentials.Username,
                Password = settings.EsbCredentials.Password,
                DispatchConsumersAsync = true
            };
        });

        // data decoder
        serviceRegister.Register<IDataDecoder>(provider => provider.GetService<IDataFormatter>());

        var runnableSettings = IocEngineConfig.RunnableSettings ?? throw new ApplicationException("No runnable settings available");

        // descriptor instance registration.
        serviceRegister.Register(runnableSettings.Descriptor.GetType(), () => runnableSettings.Descriptor);

        var descType = runnableSettings.Descriptor.GetType();

        if (descType == typeof(RoutingBridgeDescriptor))
        {
            serviceRegister = this.RegisterRoutingBridge(serviceRegister);
        }
        else if (descType == typeof(SubscriberBoundDescriptor))
        {
            serviceRegister = this.RegisterSubscriber(serviceRegister);
        }
        else
        {
            // nothing to do ..
        }

        serviceRegister.RegisterInitializer<IAsyncSubscriber<ComplexCls>>((provider, bound) =>
        {
            // todo: serializer could be json.net
            var options = provider.GetService<JsonSerializerOptions>();
            string Func(object obj) => JsonSerializer.Serialize(obj, options);

            bound.Received.Add((sender, args) => Handlers.BoundOnReceived1Async(sender, args, Func));

            bound.OnException.Add(Handlers.BoundOnOnException);

        });
    }

    private IServiceRegister RegisterRoutingBridge(IServiceRegister serviceRegister)
    {
        return serviceRegister.RegisterResolver<IAsyncRunnable, RoutingBridgeBound<ComplexCls>>()
            .Register<RoutingBridgeChannels>()
            ;
    }

    private IServiceRegister RegisterSubscriber(IServiceRegister serviceRegister)
    {
        return serviceRegister.RegisterResolver<IAsyncRunnable, SubscriberBound<ComplexCls>>()
            .Register<IConnection>(provider => provider.GetService<IConnectionFactory>()
                .BuildConnection(factory => factory.CreateConnection("host_subscriber")))
            .Register<IModel>(provider => provider.GetService<IConnection>().CreateModel())
            ;
    }
}
