using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using ServiceBus.Core.Formatters;
using System.Text;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;
using System.ComponentModel;
using System;

namespace ServiceBus.Demo.SubHost.Modules;

[Obsolete]
[Description("Obsolete only not to load in DI.")]
internal class JsonDataFormatterModule : IServiceModule
{
    public void Initialize(IServiceRegister serviceRegister)
    {
        serviceRegister.Register<IDataFormatter, RwJsonDataFormatter>()
            .Register(() => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                FloatParseHandling = FloatParseHandling.Decimal,
                NullValueHandling = NullValueHandling.Ignore
            })
            .Register(provider => JsonSerializer.CreateDefault(provider.GetService<JsonSerializerSettings>()))
            .Register(() => Encoding.UTF8);
    }
}
