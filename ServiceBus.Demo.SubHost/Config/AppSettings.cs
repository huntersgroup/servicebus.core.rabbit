using System;
using ServiceBus.Core.Clients;

namespace ServiceBus.Demo.SubHost.Config;

internal class AppSettings
{
    public Uri EsbAddress { get; set; }

    public Credentials EsbCredentials { get; set; }

    public RabbitDescriptors Descriptors { get; set; }
}
