using System;

namespace ServiceBus.Demo.SubHost.Config;

/// <summary>
/// 
/// </summary>
public class RunnableSettings
{
    /// <summary>
    /// Gets or sets the service type for this object
    /// </summary>
    public Type ServiceType { get; set; }

    /// <summary>
    /// Gets or sets the Descriptor object for the given <see cref="ServiceType"/>.
    /// </summary>
    public object Descriptor { get; set; }
}
