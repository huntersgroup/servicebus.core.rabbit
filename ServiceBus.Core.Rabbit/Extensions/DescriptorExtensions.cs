using System;
using System.Collections.Generic;
using System.Linq;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Model;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// Represents a custom way to extend some features for Descriptors instances
/// </summary>
public static class DescriptorExtensions
{
    private const string NullPropertyMessage = "The {0} property cannot be null.";
    private const string NullOrEmptyPropertyMessage = "The {0} property cannot be null or empty.";
    private const string QueueTypeAttr = "x-queue-type";
    private const string QueueTypeClassic = "classic";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static ExchangeDescriptor BuildDlxExchange(this RetryPolicyDescriptor retryPolicy, QueueDescriptor queue)
    {
        return new ExchangeDescriptor
        {
            Name = $"{queue.Name}/_dlx",
            Type = ExchangeTypes.Direct,
            Durable = retryPolicy.Durable,
            AutoDelete = retryPolicy.AutoDelete
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static ExchangeDescriptor BuildRetryExchange(this RetryPolicyDescriptor retryPolicy, QueueDescriptor queue)
    {
        return new ExchangeDescriptor
        {
            Name = $"{queue.Name}/_retry",
            Type = ExchangeTypes.Direct,
            Durable = retryPolicy.Durable,
            AutoDelete = retryPolicy.AutoDelete
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <param name="dlxExchange"></param>
    /// <returns></returns>
    public static QueueDescriptor BuildRetryQueue(this RetryPolicyDescriptor retryPolicy, QueueDescriptor queue, ExchangeDescriptor dlxExchange)
    {
        return new QueueDescriptor
        {
            Name = $"{queue.Name}/_retry",
            Durable = retryPolicy.Durable,
            AutoDelete = retryPolicy.AutoDelete,
            Arguments = new Dictionary<string, object>
            {
                {"x-message-ttl", Convert.ToInt64(retryPolicy.Delayed.TotalMilliseconds)},
                {"x-dead-letter-exchange", dlxExchange.GetChannelName()},
                {QueueTypeAttr, queue.Arguments != null && queue.Arguments.ContainsKey(QueueTypeAttr) ? queue?.Arguments[QueueTypeAttr]: QueueTypeClassic}
            }
        };
    }

    /// <summary>
    /// Verifies current status of the given descriptor instance.
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this BrokerDescriptor descriptor)
    {
        if (descriptor.Address == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Address)}"));
        }

        if (descriptor.Credentials == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Credentials)}"));
        }

        Verify(descriptor as dynamic);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this PublisherBoundDescriptor descriptor)
    {
        descriptor.ThrowIfNull(nameof(descriptor));

        if (descriptor.Exchanges == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Exchanges)}"));
        }

        descriptor.Exchanges.RemoveAll(boundDescriptor => boundDescriptor == null);

        if (!descriptor.Exchanges.Any())
        {
            descriptor.ThrowException("There is no exchange for the given descriptor, at least there should be a one.");
        }

        // verify duplicity
        descriptor.Exchanges.ThrowOnDuplicity(descriptor, string.Empty);

        var index = -1;

        foreach (var exchange in descriptor.Exchanges)
        {
            index++;
            descriptor.Verify(exchange, $"{nameof(descriptor.Exchanges)}[{index}]");
        }
    }

    /// <summary>
    /// Validates the given descriptor
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this SubscriberBoundDescriptor descriptor)
    {
        descriptor.ThrowIfNull(nameof(descriptor));

        if (descriptor.PrefetchCount < 1)
        {
            descriptor.ThrowException($"The {nameof(descriptor.PrefetchCount)} property cannot be less than 1.");
        }

        if (descriptor.Queue == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Queue)}"));
        }
        else
        {
            descriptor.Queue.VerifyStatus(string.Empty, descriptor);
        }

        descriptor.Exchange?.VerifyStatus(string.Empty, descriptor);

        descriptor.RetryPolicy?.VerifyStatus(string.Empty, descriptor);
    }

    private static void VerifyStatus(this RetryPolicyDescriptor descriptor, string parentProperty, IBrokerDescriptor brokerDescriptor)
    {
        if (descriptor == null) return;

        if (descriptor.Enabled)
        {
            if (descriptor.MaxRetry < 1)
            {
                brokerDescriptor.ThrowException($"The {parentProperty}.{nameof(descriptor.MaxRetry)} property cannot be less than 1.");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="exchangeBound"></param>
    /// <param name="rootParent"></param>
    private static void Verify(this IBrokerDescriptor descriptor, ExchangeBoundDescriptor exchangeBound, string rootParent)
    {
        var index = -1;

        if (exchangeBound.Queues != null)
        {
            exchangeBound.Queues.RemoveAll(boundDescriptor => boundDescriptor == null);

            exchangeBound.Queues.ThrowOnDuplicity(descriptor, rootParent);

            foreach (var queue in exchangeBound.Queues)
            {
                index++;
                queue.VerifyStatus($"{rootParent}.{nameof(exchangeBound.Queues)}[{index}]", descriptor);
            }
        }

        index = -1;
        if (exchangeBound.Exchanges != null)
        {
            exchangeBound.Exchanges.RemoveAll(boundDescriptor => boundDescriptor == null);

            exchangeBound.Exchanges.ThrowOnDuplicity(descriptor, rootParent);

            foreach (var exchange in exchangeBound.Exchanges)
            {
                index++;
                descriptor.Verify(exchange, $"{rootParent}.{nameof(exchangeBound.Exchanges)}[{index}]");
            }
        }

        exchangeBound.VerifyStatus(rootParent, descriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    // ReSharper disable once UnusedParameter.Local
    private static void Verify(BrokerDescriptor descriptor)
    {
        // nothing to do !!!
        // this is a fallback method to execute whenever there's no overload to execute using dynamics.
    }
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(BinderSubscriberDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Binder == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Binder)}"));
        }

        if (descriptor.Exchange == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Exchange)}"));
        }

        descriptor.Exchange.VerifyStatus($"{nameof(descriptor.Exchange)}", descriptor);

        Verify(descriptor as SubscriberDescriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(SubscriberDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Queue == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Queue)}"));
        }

        descriptor.Queue.VerifyStatus($"{nameof(descriptor.Queue)}", descriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(BinderPublisherDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Binder == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Binder)}"));
        }

        if (descriptor.Queue == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Queue)}"));
        }

        descriptor.Queue.VerifyStatus($"{nameof(descriptor.Queue)}", descriptor);

        Verify(descriptor as PublisherDescriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(PublisherDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Exchange == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Exchange)}"));
        }

        if (descriptor.OnBuildProperties == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.OnBuildProperties)}"));
        }

        descriptor.Exchange.VerifyStatus($"{nameof(descriptor)}.{nameof(descriptor.Exchange)}", descriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceDescriptor"></param>
    /// <param name="parentProperty"></param>
    /// <param name="descriptor"></param>
    private static void VerifyStatus(this IServiceDescriptor serviceDescriptor, string parentProperty, IBrokerDescriptor descriptor)
    {
        if (string.IsNullOrWhiteSpace(serviceDescriptor.Name))
        {
            descriptor.ThrowException(string.Format(NullOrEmptyPropertyMessage, $"{parentProperty}.{nameof(serviceDescriptor.Name)}"));
        }

        if (string.IsNullOrWhiteSpace(serviceDescriptor.Prefix))
        {
            descriptor.ThrowException(string.Format(NullOrEmptyPropertyMessage, $"{parentProperty}.{nameof(serviceDescriptor.Prefix)}"));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="message"></param>
    private static void ThrowException(this IBrokerDescriptor descriptor, string message)
    {
        throw new InvalidDescriptorException(message)
        {
            ClientType = descriptor.GetType()
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptors"></param>
    /// <param name="parent"></param>
    /// <param name="propertyPath"></param>
    /// <exception cref="DuplicateDescriptorException"></exception>
    private static void ThrowOnDuplicity(this IEnumerable<IServiceDescriptor> descriptors, IBrokerDescriptor parent, string propertyPath)
    {
        var res = descriptors.GroupBy(desc => desc.GetChannelName())
            .Where(grouping => grouping.Count() > 1)
            .ToList();

        if (res.Any())
        {
            throw new DuplicateDescriptorException("Some descriptors with the same full name were found, see exception details.")
            {
                ClientType = descriptors.GetType(),
                PropertyPath = propertyPath,
                Names = res.Select(grouping => grouping.Key).ToList(),
                Type = descriptors.FirstOrDefault()?.GetType()
            };
        }
    }
}
