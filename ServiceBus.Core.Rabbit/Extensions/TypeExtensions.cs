using System;
using System.Collections.Generic;
using ServiceBus.Core.Model;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// Represents common extension methods for types.
/// </summary>
public static class TypeExtensions
{
    private static readonly IDictionary<ExchangeTypes, string> ExchangeDic = new Dictionary<ExchangeTypes, string>
    {
        { ExchangeTypes.Direct, "direct" },
        { ExchangeTypes.Fanout, "fanout" },
        { ExchangeTypes.Topic, "topic" },
        { ExchangeTypes.Headers, "headers" }
    };

    /// <summary>
    /// Gets the exchange type name translated by the given <see cref="ExchangeTypes"/> instance.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string GetName(this ExchangeTypes type)
    {
        return ExchangeDic[type];
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="datetime"></param>
    /// <returns></returns>
    public static long AsUnixDateTime(this DateTime datetime)
    {
        var datetimeOffset = new DateTimeOffset(datetime);
        return datetimeOffset.ToUnixTimeSeconds();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="unixSecondsTime"></param>
    /// <returns></returns>
    public static DateTime FromUnixDateTime(this long unixSecondsTime)
    {
        var dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(unixSecondsTime);
        return dateTimeOffset.DateTime;
    }

    /// <summary>
    /// Counts the number of delegates.
    /// </summary>
    /// <typeparam name="TEvent"></typeparam>
    /// <param name="eventHandler"></param>
    /// <returns>Returns zero even if the current instance is null.</returns>
    public static int Count<TEvent>(this EventHandler<TEvent> eventHandler)
    {
        var delegates = eventHandler?.GetInvocationList();

        return delegates?.Length ?? 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TInterface"></typeparam>
    /// <param name="type"></param>
    /// <returns></returns>
    public static bool Implements<TInterface>(this Type type)
    {
        return type.Implements(typeof(TInterface));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="interfaceType"></param>
    /// <returns></returns>
    public static bool Implements(this Type type, Type interfaceType)
    {
        if (interfaceType.IsAssignableFrom(type) || interfaceType.GetOpenGenericOrItSelf().IsAssignableFrom(type.GetOpenGenericOrItSelf()))
        {
            return true;
        }

        var allInterfaceTypes = type.GetInterfaces();

        foreach (var current in allInterfaceTypes)
        {
            if (interfaceType == current || interfaceType.GetOpenGenericOrItSelf() == current.GetOpenGenericOrItSelf())
            {
                return true;
            }
        }

        return false;
    }

    private static Type GetOpenGenericOrItSelf(this Type type)
    {
        return type.IsGenericType ? type.GetGenericTypeDefinition() : type;
    }
}
