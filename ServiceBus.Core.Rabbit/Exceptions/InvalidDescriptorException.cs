using System;

namespace ServiceBus.Core.Exceptions;

/// <summary>
/// Represents a generic way to indicate an exception thrown whenever a descriptor is initialized with wrong status.
/// </summary>
public class InvalidDescriptorException : Exception
{
    /// <summary>
    /// A common ctor for this exception.
    /// </summary>
    /// <param name="message"></param>
    public InvalidDescriptorException(string message)
        :base(message)
    {
    }

    /// <summary>
    /// A common ctor for this exception.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    public InvalidDescriptorException(string message, Exception innerException)
        : base(message, innerException)
    {
    }

    /// <summary>
    /// Gets or sets the client type that throws the exception.
    /// </summary>
    public Type ClientType { get; set; }
}
