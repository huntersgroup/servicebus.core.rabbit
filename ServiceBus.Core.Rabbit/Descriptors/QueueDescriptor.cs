using System.Collections.Generic;
using System.Diagnostics;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a queue descriptor.
/// </summary>
[DebuggerDisplay("prefix: {this.Prefix}, name: {this.Name}, durable: {this.Durable}, exclusive: {this.Exclusive}, auto-delete: {this.AutoDelete}")]
public class QueueDescriptor : IServiceDescriptor
{
    ///<inheritdoc/>
    public string Prefix { get; set; } = "esb/queue";

    ///<inheritdoc/>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets a boolean value indicating if queue must be persisted on disk.
    /// </summary>
    public bool Durable { get; set; } = false;

    /// <summary>
    /// Gets or sets a boolean value indicating if this queue is exclusively used by one subscriber.
    /// </summary>
    public bool Exclusive { get; set; } = false;

    /// <summary>
    /// Gets or sets a boolean value indicating if queue could be cancelled after subscribers lose binding connection.
    /// <para>This feature is enabled when <see cref="Durable"/> value is false.</para> 
    /// </summary>
    public bool AutoDelete { get; set; } = true;

    ///<inheritdoc/>
    public IDictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>();
}
