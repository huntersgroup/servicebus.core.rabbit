namespace ServiceBus.Core.Descriptors;

/// <summary>
/// 
/// </summary>
public interface IBrokerDescriptor
{
    /// <summary>
    /// Gets a boolean value used to identify if channel events must be registered
    /// </summary>
    bool EnableChannelEvents { get; }
}
