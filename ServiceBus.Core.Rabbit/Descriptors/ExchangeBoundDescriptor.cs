using System.Collections.Generic;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// 
/// </summary>
public class ExchangeBoundDescriptor : ExchangeDescriptor
{
    /// <summary>
    /// Gets or set a binder to use with <see cref="Exchanges"/> instances.
    /// </summary>
    public BinderDescriptor Binder { get; set; } = new();

    /// <summary>
    /// Gets or sets 
    /// </summary>
    public List<QueueBoundDescriptor> Queues { get; set; } = new();

    /// <summary>
    /// 
    /// </summary>
    public List<ExchangeBoundDescriptor> Exchanges { get; set; } = new();
}
