namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a way to bind <see cref="Queue"/> with <see cref="Exchange"/> service using a custom <see cref="Binder"/>.
/// </summary>
public interface IExchangeBinderDescriptor
{
    /// <summary>
    /// Gets a exchange descriptor.
    /// </summary>
    ExchangeDescriptor Exchange { get; }

    /// <summary>
    /// Gets a queue descriptor.
    /// </summary>
    QueueDescriptor Queue { get; }

    /// <summary>
    /// Gets a custom binder for the current <see cref="Exchange"/> with <see cref="Queue"/>.
    /// </summary>
    BinderDescriptor Binder { get; }
}
