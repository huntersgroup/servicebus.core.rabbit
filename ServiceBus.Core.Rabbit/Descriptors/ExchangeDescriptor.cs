using System.Collections.Generic;
using System.Diagnostics;
using ServiceBus.Core.Model;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents common information for exchanges definition.
/// </summary>
[DebuggerDisplay("prefix: {this.Prefix}, name: {this.Name}, durable: {this.Durable}, auto-delete: {this.AutoDelete}")]
public class ExchangeDescriptor : IServiceDescriptor
{
    ///<inheritdoc/>
    public string Prefix { get; set; } = "esb/exchange";

    ///<inheritdoc/>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets a boolean value indicating if exchange must be persisted on disk.
    /// </summary>
    public bool Durable { get; set; } = false;

    /// <summary>
    /// Gets or sets a boolean value indicating if exchange could be cancelled after subscribers lose binding connection with queues.
    /// <para>This feature is enabled when <see cref="Durable"/> value is false.</para> 
    /// </summary>
    public bool AutoDelete { get; set; } = false;

    /// <summary>
    /// Gets or sets the type of exchange.
    /// </summary>
    public ExchangeTypes Type { get; set; }

    ///<inheritdoc/>
    public IDictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>();
}
