using System.Collections.Generic;
using System.Diagnostics;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a binder descriptor for queues and exchanges.
/// </summary>
[DebuggerDisplay("routingKey: {this.RoutingKey}")]
public class BinderDescriptor
{
    /// <summary>
    /// 
    /// </summary>
    public string RoutingKey { get; set; } = string.Empty;

    /// <summary>
    /// 
    /// </summary>
    public IDictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>();
}
