using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Clients.Subscribers.Descriptors;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// 
/// </summary>
public class RoutingBridgeDescriptor
{
    /// <summary>
    /// 
    /// </summary>
    public SubscriberBoundDescriptor Source { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public PublisherBoundDescriptor Target { get; set; }
}
