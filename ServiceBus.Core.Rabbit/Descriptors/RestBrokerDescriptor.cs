namespace ServiceBus.Core.Descriptors;

/// <summary>
/// 
/// </summary>
public class RestBrokerDescriptor : BrokerDescriptor
{
    /// <summary>
    /// Gets or sets the service name that identifies micro service component which hosts a subscriber.
    /// </summary>
    public string ServiceName { get; set; }

    /// <summary>
    /// Gets or sets a boolean value indicating if channel components are persistent.
    /// </summary>
    public bool Durable { get; set; }
}
