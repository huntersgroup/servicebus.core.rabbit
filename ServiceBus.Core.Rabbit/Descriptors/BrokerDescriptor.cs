using System;
using ServiceBus.Core.Clients;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a basic descriptor used to configure publishers ans subscribers instances.
/// </summary>
public class BrokerDescriptor : IBrokerDescriptor
{
    /// <summary>
    /// Gets or sets the current virtual host for publishers.
    /// </summary>
    public string VirtualHost { get; set; } = "/";

    /// <summary>
    /// Gets or sets the current uri indicating the address for Rabbit cluster.
    /// </summary>
    public Uri Address { get; set; }

    /// <summary>
    /// Gets or sets the credentials used to authenticate with rabbit broker. 
    /// </summary>
    public Credentials Credentials { get; set; }

    /// <summary>
    /// Gets or sets a boolean value used to identify if channel event must be registered
    /// </summary>
    public bool EnableChannelEvents { get; set; }
}
