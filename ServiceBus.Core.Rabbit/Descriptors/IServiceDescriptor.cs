using System.Collections.Generic;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a common way to indicate a common service descriptor.
/// </summary>
public interface IServiceDescriptor
{
    /// <summary>
    /// Gets or sets the common prefix used for naming the current service descriptor.
    /// </summary>
    string Prefix { get; }

    /// <summary>
    /// Gets or sets the name of this service descriptor.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Gets or sets broker arguments for the definition of service.
    /// </summary>
    // ReSharper disable once UnusedMemberInSuper.Global
    IDictionary<string, object> Arguments { get; }
}
