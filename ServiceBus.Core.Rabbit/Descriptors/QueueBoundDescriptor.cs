namespace ServiceBus.Core.Descriptors;

/// <summary>
/// 
/// </summary>
public class QueueBoundDescriptor : QueueDescriptor
{
    /// <summary>
    /// 
    /// </summary>
    public BinderDescriptor Binder { get; set; } = new();
}
