using System.Collections.Generic;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Clients.Subscribers.Descriptors;

/// <summary>
/// Represents a basic descriptor for <see cref="CommonSubscriber{TData}"/> instances.
/// </summary>
public class SubscriberDescriptor : BrokerDescriptor
{
    /// <summary>
    /// Gets or sets the format of incoming messages
    /// </summary>
    public MessageFormat MessageFormat { get; set; } = MessageFormat.Header;

    /// <summary>
    /// Gets or sets the prefetch value used to subscribers.
    /// <para>
    /// The default value is 1.
    /// </para>
    /// </summary>
    public ushort PrefetchCount { get; set; } = 1;
        
    /// <summary>
    /// Gets or sets the queue descriptor.
    /// </summary>
    public QueueDescriptor Queue { get; set; } = new();

    /// <summary>
    /// Gets or sets policy used to retry sending messages whenever they're nack or reject status.
    /// </summary>
    public RetryPolicyDescriptor RetryPolicy { get; set; } = new();

    /// <summary>
    /// Gets or sets a custom parameters used to pass parameters between channels and consumers.
    /// </summary>
    public IDictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>();
}
