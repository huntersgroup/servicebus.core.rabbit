using System;
using System.Threading.Tasks;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Clients.Consumers;

/// <summary>
/// Represents a simple message consumer that doesn't subscribe in queues, but download an specific number of messages by <see cref="SubscriberDescriptor.PrefetchCount"/> property.
/// </summary>
/// <typeparam name="TData"></typeparam>
public class BasicConsumer<TData> : BrokerClient, IConsumer<TData>
{
    private readonly SubscriberDescriptor descriptor;
    private readonly Func<BasicDeliverEventArgs, Message<TData>> messageDecoderFunc;
    private ExchangeDescriptor dlxExchange;
    private Action<object, BasicDeliverEventArgs> messageAction;

    /// <summary>
    /// Creates a new instance of <see cref="BasicConsumer{TData}"/> instance.
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="decoder"></param>
    public BasicConsumer(SubscriberDescriptor descriptor, IDataDecoder decoder)
        : base(descriptor.ExecuteCustomAction(() => decoder.ThrowIfNull(nameof(decoder))))
    {
        this.descriptor = descriptor;

        this.SetUp();

        this.messageDecoderFunc = descriptor.MessageFormat switch
        {
            MessageFormat.Header => ea => decoder.Decode<Message<TData>>(ea.Body),
            MessageFormat.Headless => ea =>
            {
                var body = decoder.Decode<TData>(ea.Body);

                var properties = ea.BasicProperties;

                return new Message<TData>
                {
                    Route = ea.RoutingKey,
                    Persistent = properties?.Persistent,
                    Created = properties?.Timestamp.UnixTime.FromUnixDateTime(),
                    Data = body
                };
            }
            ,
            _ => throw new InvalidDescriptorException(
                $"The current message format indicated in the current subscriber descriptor is not supported, value: {descriptor.MessageFormat}")
        };
    }

    ///<inheritdoc/>
    public event EventHandler<MessageDeliverEventArgs<TData>> Received;

    ///<inheritdoc/>
    public event EventHandler<MessageExceptionEventArgs> OnException;

    ///<inheritdoc/>
    public event EventHandler<RetryExceptionEventArgs<TData>> OnMaxRetryFailed;

    ///<inheritdoc/>
    public Task Execute()
    {
        if (this.Received == null)
        {
            throw new SetupBrokerClientException($"The {nameof(this.Received)} event handler must be set before calling this method.");
        }

        var queueName = this.descriptor.Queue.GetChannelName();

        for (var i = 0; i < this.descriptor.PrefetchCount; i++)
        {
            var queueMessage = this.Channel.BasicGet(queueName, false);

            if (queueMessage == null)
            {
                break;
            }

            var ea = new BasicDeliverEventArgs(string.Empty, queueMessage.DeliveryTag, queueMessage.Redelivered,
                queueMessage.Exchange, queueMessage.RoutingKey,
                queueMessage.BasicProperties, queueMessage.Body);

            try
            {
                this.messageAction(this.Channel, ea);
            }
            catch (Exception ex)
            {
                this.CatchException(this, ex, ea);
            }
        }

        return Task.CompletedTask;
    }

    private void SetUp()
    {
        try
        {
            // prefetch in this case must be set to one, there's no sense to download many messages and elaborating many ones at the same time.
            this.Channel.BasicQos(0, 1, false);

            this.Channel
                .BuildQueue(this.descriptor.Queue);

            this.dlxExchange = this.Channel.BuildDlxExchange(this.descriptor.RetryPolicy, this.descriptor.Queue);

            this.messageAction = this.dlxExchange == null ? this.OnReceived : this.OnReceivedWithRetry;
        }
        catch (Exception e)
        {
            throw new SetupBrokerClientException("An exception was thrown on setup subscriber, see inner exception for details", e)
            {
                ClientType = typeof(BasicConsumer<TData>)
            };
        }
    }

    private void CatchException(object sender, Exception ex, BasicDeliverEventArgs ea)
    {
        this.Channel.BasicReject(ea.DeliveryTag, false);

        this.OnException?.Invoke(sender, new MessageExceptionEventArgs
        {
            PayloadType = typeof(TData),
            Exception = ex
        });
    }

    private void OnReceived(object sender, BasicDeliverEventArgs ea)
    {
        var eventArg = this.GetMessageDeliverEventArgs(ea);

        this.Received?.Invoke(sender, eventArg);

        if (eventArg.Acknowledged)
        {
            this.Channel.BasicAck(ea.DeliveryTag, false);
        }
        else
        {
            this.Channel.BasicNack(ea.DeliveryTag, false, eventArg.Requeue);
        }
    }

    private void OnReceivedWithRetry(object sender, BasicDeliverEventArgs ea)
    {
        var eventArg = this.GetMessageDeliverEventArgs(ea);

        this.Received?.Invoke(sender, eventArg);

        if (eventArg.Acknowledged)
        {
            this.Channel.BasicAck(ea.DeliveryTag, false);
        }
        else
        {
            this.Channel.BasicNack(ea.DeliveryTag, false, false);

            if (eventArg.Requeue)
            {
                var retryCounter = ea.BasicProperties.IncrementRetryCounter();

                if (retryCounter < this.descriptor.RetryPolicy.MaxRetry)
                {
                    this.Channel.PublishMessage(this.dlxExchange, string.Empty, ea.BasicProperties, ea.Body);
                }
                else
                {
                    this.OnMaxRetryFailed?.Invoke(sender, new RetryExceptionEventArgs<TData>
                    {
                        Message = eventArg.Message,
                        RetryCounter = retryCounter,
                        Exception = new Exception("An exception was occurred due to reach the max retry counter for requeue message")
                    });
                }
            }
        }
    }

    private MessageDeliverEventArgs<TData> GetMessageDeliverEventArgs(BasicDeliverEventArgs ea)
    {
        var message = this.messageDecoderFunc(ea);
        return new MessageDeliverEventArgs<TData>
        {
            Message = message
        };
    }
}
