using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Clients;

/// <summary>
/// Represents a common client for Rabbit broker connection.
/// </summary>
public class BrokerClient : IBrokerClient, IDisposable
{
    /// <summary>
    /// 
    /// </summary>
    protected EventHandler<ChannelEventArgs> OnChannelEventRaised;
    private bool isDisposed;

    /// <summary>
    /// Creates a new <see cref="BrokerClient"/> instance.
    /// </summary>
    /// <param name="descriptor"></param>
    public BrokerClient(BrokerDescriptor descriptor)
    {
        if (descriptor == null)
        {
            throw new ArgumentNullException(nameof(descriptor), "The given descriptor cannot be null.");
        }

        descriptor.VerifyStatus();

        var connectionFactory = new ConnectionFactory
        {
            Uri = descriptor.Address,
            VirtualHost = descriptor.VirtualHost,
            UserName = descriptor.Credentials?.Username,
            Password = descriptor.Credentials?.Password
        };

        this.Connection = connectionFactory.CreateConnection();
        this.Channel = this.Connection.CreateModel();

        // ReSharper disable once InvertIf
        if (descriptor.EnableChannelEvents)
        {
            this.Channel.FlowControl += this.OnChannelFlowControl;
            this.Channel.CallbackException += this.OnChannelCallbackException;
            this.Channel.ModelShutdown += this.OnChannelModelShutdown;
        }
    }

    ///<inheritdoc/>
    public event EventHandler<ChannelEventArgs> OnChannelEventFired
    {
        add
        {
            if (value == null) return;
            this.OnChannelEventRaised += value;
        }
        remove
        {
            if (value == null) return;
            this.OnChannelEventRaised -= value;
        }
    }

    /// <summary>
    /// Gets the underlying rabbit connection.
    /// </summary>
    protected IConnection Connection { get; }

    /// <summary>
    /// Gets the underlying rabbit channel.
    /// </summary>
    protected IModel Channel { get; }

    ///<inheritdoc/>
    void IDisposable.Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Disposes the current instance if disposing parameter is true.
    /// </summary>
    /// <param name="disposing"></param>
    protected virtual void Dispose(bool disposing)
    {
        if (this.isDisposed)
        {
            return;
        }

        if (disposing)
        {
            try
            {
                this.Channel?.Close();
                this.Connection?.Close();
            }
            finally
            {
                this.isDisposed = true;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    protected void ThrowIfDisposed()
    {
        var type = this.GetType();

        if (this.isDisposed)
        {
            throw new ObjectDisposedException($"The current {type.Name} instance was disposed, type: {type.FullName}");
        }
    }
        
    private void OnChannelFlowControl(object sender, FlowControlEventArgs e)
    {
        if (this.OnChannelEventRaised == null) return;

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.FlowControl,
            Properties =
            {
                {nameof(e.Active), e.Active}
            }
        };

        this.OnChannelEventRaised.Invoke(sender, args);
    }

    private void OnChannelCallbackException(object sender, CallbackExceptionEventArgs e)
    {
        if (this.OnChannelEventRaised == null) return;

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.CallbackException,
            Properties =
            {
                {nameof(e.Detail), e.Detail},
                {nameof(e.Exception), e.Exception}
            }
        };

        this.OnChannelEventRaised.Invoke(sender, args);
    }

    private void OnChannelModelShutdown(object sender, ShutdownEventArgs e)
    {
        if (this.OnChannelEventRaised == null) return;

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.ModelShutdown,
            Properties =
            {
                {nameof(e.ClassId), e.ClassId},
                {nameof(e.MethodId), e.MethodId},
                {nameof(e.ReplyCode), e.ReplyCode},
                {nameof(e.ReplyText), e.ReplyText}
            }
        };

        this.OnChannelEventRaised.Invoke(sender, args);
    }
}
