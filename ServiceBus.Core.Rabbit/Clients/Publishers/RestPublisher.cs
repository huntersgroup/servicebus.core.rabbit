using System;
using System.Threading.Tasks;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// A common client used to communicate with Rest services via RabbitMq channel.
/// </summary>
public class RestPublisher : IRestPublisher, IDisposable
{
    private readonly IRestPublisher internalPub;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="dataFormatter"></param>
    /// <param name="restMessageSerializer"></param>
    public RestPublisher(RestPublisherDescriptor descriptor, IDataFormatter dataFormatter, IHttpRestMessageSerializer restMessageSerializer)
    {
        if (descriptor.FullDuplex)
        {
            var pub = new FullDuplexRestPublisher(descriptor, dataFormatter, restMessageSerializer);
            this.DataFormatter = pub.DataFormatter;
            this.internalPub = pub;
        }
        else
        {
            var pub2 = new StatelessRestPublisher(descriptor, dataFormatter, restMessageSerializer);
            this.DataFormatter = pub2.DataFormatter;
            this.internalPub = pub2;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public IDataFormatter DataFormatter { get; }

    /// <inheritdoc/>
    public Task<RestResponse> SendAsync(RestRequest request)
    {
        return this.internalPub.SendAsync(request);
    }

    /// <inheritdoc/>
    public Task<RestResponse<TData>> SendAsync<TData>(RestRequest request)
    {
        return this.internalPub.SendAsync<TData>(request);
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        (this.internalPub as IDisposable)?.Dispose();
    }

    /// <inheritdoc/>
    public event EventHandler<ChannelEventArgs> OnChannelEventFired;
}
