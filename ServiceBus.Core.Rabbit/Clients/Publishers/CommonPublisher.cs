using System;
using System.Globalization;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// Represents a basic publisher.
/// </summary>
public class CommonPublisher : BrokerClient, IPublisher
{
    private readonly PublisherDescriptor descriptor;
    private readonly IDataEncoder encoder;
        
    /// <summary>
    /// Creates a new <see cref="CommonPublisher"/> instance.
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="encoder"></param>
    public CommonPublisher(PublisherDescriptor descriptor, IDataEncoder encoder)
        :base(descriptor.ExecuteCustomAction(() => encoder.ThrowIfNull(nameof(encoder))))
    {
        this.descriptor = descriptor;
        this.encoder = encoder ?? throw new ArgumentNullException(nameof(encoder), $"The given {nameof(IDataEncoder)} cannot be null.");
            
        try
        {
            this.Channel
                .BuildExchange(this.descriptor.Exchange);

            // ReSharper disable once InvertIf
            if (descriptor.Confirmation.Enabled)
            {
                this.Channel.ConfirmSelect();

                this.Channel.BasicReturn += this.OnChannelBasicReturn;
                this.Channel.BasicAcks += this.OnChannelBasicAcks;
                this.Channel.BasicNacks += this.OnChannelBasicNacks;
            }
        }
        catch (Exception e)
        {
            throw new SetupBrokerClientException("An exception was thrown on setup publisher, see inner exception for details", e)
            {
                ClientType = typeof(CommonPublisher)
            };
        }
    }
        
    ///<inheritdoc/>
    public void Send(object data)
    {
        if (data is IMessage<dynamic> msg)
        {
            this.Send(msg);
        }
        else
        {
            var message = new Message<object>
            {
                Created = DateTime.UtcNow,
                Data = data
            };

            this.Send(message);
        }
    }

    ///<inheritdoc/>
    public void Send<TData>(IMessage<TData> message)
    {
        this.ThrowIfDisposed();

        this.Evaluate(message);

        var exchange = this.descriptor.Exchange;
        var body = this.BuildMessage(message);
            
        lock (this.Channel)
        {
            var properties = this.CreateProperties(message);

            this.Channel.PublishMessage(exchange, message.Route ?? string.Empty, properties, body);
        }
    }

    ///<inheritdoc/>
    public Task SendAsync(object data)
    {
        return Task.Factory.StartNew(() => this.Send(data));
    }

    ///<inheritdoc/>
    public Task SendAsync<TData>(IMessage<TData> message)
    {
        return Task.Factory.StartNew(() => this.Send(message));
    }

    ///<inheritdoc/>
    protected override void Dispose(bool disposing)
    {
        if (this.descriptor.Confirmation.Enabled && this.descriptor.Confirmation.WaitFor)
        {
            var timeout = this.descriptor.Confirmation.Timeout ?? TimeSpan.FromMinutes(1);
            this.Channel.WaitForConfirms(timeout);
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="message"></param>
    private void Evaluate<TData>(IMessage<TData> message)
    {
        if (message == null)
        {
            throw new ArgumentNullException(nameof(message), "The given message cannot be null.");
        }

        message.Created ??= DateTime.UtcNow;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private IBasicProperties CreateProperties(IMessage message)
    {
        var ttl = message.Ttl ?? this.descriptor.Message.Ttl ?? TimeSpan.FromMinutes(15);

        var properties = this.Channel.CreateBasicProperties();

        if (!string.IsNullOrWhiteSpace(message.Id))
        {
            properties.MessageId = message.Id;
        }

        message.Tag = this.Channel.NextPublishSeqNo.ToString();
        properties.Timestamp = new AmqpTimestamp((message.Created ?? DateTime.UtcNow).AsUnixDateTime());
        properties.Expiration = ttl.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);
        properties.Persistent = message.Persistent ?? this.descriptor.Message.Persistent;
            
        this.descriptor.OnBuildProperties(properties);

        return properties;
    }

    private byte[] BuildMessage<TData>(IMessage<TData> message)
    {
        return this.descriptor.Message.Format switch
        {
            MessageFormat.Header => this.encoder.Encode(message),
            MessageFormat.Headless => this.encoder.Encode(message.Data),
            _ => throw new InvalidDescriptorException($"The current message format indicated in the current publisher message descriptor is not supported, value: {descriptor.Message.Format}")
        };
    }
        
    private void OnChannelBasicReturn(object sender, BasicReturnEventArgs e)
    {
        if (this.OnChannelEventRaised == null)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Return,
            Properties =
            {
                {nameof(e.ReplyText), e.ReplyText},
                {nameof(e.ReplyCode), e.ReplyCode},
                {nameof(e.BasicProperties), e.BasicProperties},
                {nameof(e.Exchange), e.Exchange},
                {nameof(e.RoutingKey), e.RoutingKey}
            }
        };

        this.OnChannelEventRaised.Invoke(sender, args);
    }

    private void OnChannelBasicAcks(object sender, BasicAckEventArgs e)
    {
        if (this.OnChannelEventRaised == null)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Acks,
            Properties =
            {
                {nameof(e.DeliveryTag), e.DeliveryTag},
                {nameof(e.Multiple), e.Multiple}
            }
        };

        this.OnChannelEventRaised.Invoke(sender, args);
    }

    private void OnChannelBasicNacks(object sender, BasicNackEventArgs e)
    {
        if (this.OnChannelEventRaised == null)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Nacks,
            Properties =
            {
                {nameof(e.DeliveryTag), e.DeliveryTag},
                {nameof(e.Multiple), e.Multiple},
                {nameof(e.Requeue), e.Requeue}
            }
        };

        this.OnChannelEventRaised.Invoke(sender, args);
    }
}
