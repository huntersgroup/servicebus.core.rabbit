using ServiceBus.Core.Formatters;
using System;
using System.Threading.Tasks;
using RabbitMQ.Client;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Extensions;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using System.Globalization;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// Represents a common way to forward messages into one or more exchanges.
/// <para>
/// This publisher can forward messages into <see cref="PublisherBoundDescriptor.Exchanges"/>, at the same time, each <see cref="ExchangeBoundDescriptor"/> instance could
/// bind with other exchanges and queues, when It's bound with a <see cref="ExchangeBoundDescriptor"/> instance, this operation could be recursive, composing a large routing strategy.
/// </para>
/// </summary>
public class PublisherBound : BoundClient, IAsyncPublisher, IDisposable
{
    private readonly PublisherBoundDescriptor descriptor;
    private readonly IDataEncoder encoder;
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="encoder"></param>
    /// <param name="channel"></param>
    /// <exception cref="SetupBrokerClientException"></exception>
    public PublisherBound(PublisherBoundDescriptor descriptor, IDataEncoder encoder, IModel channel)
        : base(descriptor, channel)
    {
        descriptor.ThrowIfNull(nameof(descriptor));
        encoder.ThrowIfNull(nameof(encoder));
            
        this.descriptor = descriptor;
        this.encoder = encoder;

        this.descriptor.VerifyStatus();

        try
        {
            if (this.descriptor.Confirmation.Enabled)
            {
                this.Channel.ConfirmSelect();

                this.Channel.BasicReturn += this.OnChannelBasicReturn;
                this.Channel.BasicAcks += this.OnChannelBasicAcks;
                this.Channel.BasicNacks += this.OnChannelBasicNacks;
            }

            foreach (var exchange in descriptor.Exchanges)
            {
                this.Channel.BuildExchangeBound(exchange);
            }
        }
        catch (Exception e)
        {
            throw new SetupBrokerClientException("An exception was thrown on setup publisher, see inner exception for details", e)
            {
                ClientType = typeof(PublisherBound)
            };
        }
    }

    ///<inheritdoc/>
    public void Send(object data)
    {
        if (data is IMessage<dynamic> msg)
        {
            this.Send(msg);
        }
        else
        {
            var message = new Message<object>
            {
                Created = DateTime.UtcNow,
                Data = data
            };

            this.Send(message);
        }
    }

    ///<inheritdoc/>
    public void Send<TData>(IMessage<TData> message)
    {
        this.Evaluate(message);

        var exchanges = this.descriptor.Exchanges;
        var body = this.BuildMessage(message);

        lock (this.Channel)
        {
            var properties = this.CreateProperties(message);

            foreach (var exchange in exchanges)
            {
                this.Channel.PublishMessage(exchange, message.Route ?? string.Empty, properties, body);
            }
        }
    }

    ///<inheritdoc/>
    public Task SendAsync(object data)
    {
        return Task.Factory.StartNew(() => this.Send(data));
    }

    ///<inheritdoc/>
    public Task SendAsync<TData>(IMessage<TData> message)
    {
        return Task.Factory.StartNew(() => this.Send(message));
    }

    ///<inheritdoc/>
    public void Dispose()
    {
        if (this.descriptor.Confirmation.Enabled && this.descriptor.Confirmation.WaitFor)
        {
            var timeout = this.descriptor.Confirmation.Timeout ?? TimeSpan.FromMinutes(1);

            if (this.Channel.IsOpen)
            {
                this.Channel.WaitForConfirms(timeout);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="message"></param>
    private void Evaluate<TData>(IMessage<TData> message)
    {
        if (message == null)
        {
            throw new ArgumentNullException(nameof(message), "The given message cannot be null.");
        }

        message.Created ??= DateTime.UtcNow;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private IBasicProperties CreateProperties(IMessage message)
    {
        var ttl = message.Ttl ?? this.descriptor.Message.Ttl ?? TimeSpan.FromMinutes(15);

        var properties = this.Channel.CreateBasicProperties();

        if (!string.IsNullOrWhiteSpace(message.Id))
        {
            properties.MessageId = message.Id;
        }

        message.Tag = this.Channel.NextPublishSeqNo.ToString();
        properties.Timestamp = new AmqpTimestamp((message.Created ?? DateTime.UtcNow).AsUnixDateTime());
        properties.Expiration = ttl.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);
        properties.Persistent = message.Persistent ?? this.descriptor.Message.Persistent;

        this.descriptor.OnBuildProperties(properties);

        return properties;
    }

    private byte[] BuildMessage<TData>(IMessage<TData> message)
    {
        return this.descriptor.Message.Format switch
        {
            MessageFormat.Header => this.encoder.Encode(message),
            MessageFormat.Headless => this.encoder.Encode(message.Data),
            _ => throw new InvalidDescriptorException($"The current message format indicated in the current publisher message descriptor is not supported, value: {this.descriptor.Message.Format}")
        };
    }

    private void OnChannelBasicReturn(object sender, BasicReturnEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Return,
            Properties =
            {
                {nameof(e.ReplyText), e.ReplyText},
                {nameof(e.ReplyCode), e.ReplyCode},
                {nameof(e.BasicProperties), e.BasicProperties},
                {nameof(e.Exchange), e.Exchange},
                {nameof(e.RoutingKey), e.RoutingKey}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }

    private void OnChannelBasicAcks(object sender, BasicAckEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Acks,
            Properties =
            {
                {nameof(e.DeliveryTag), e.DeliveryTag},
                {nameof(e.Multiple), e.Multiple}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }

    private void OnChannelBasicNacks(object sender, BasicNackEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Nacks,
            Properties =
            {
                {nameof(e.DeliveryTag), e.DeliveryTag},
                {nameof(e.Multiple), e.Multiple},
                {nameof(e.Requeue), e.Requeue}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }
}
