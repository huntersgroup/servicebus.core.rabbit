using System;
using System.Collections.Generic;
using RabbitMQ.Client;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Clients.Publishers.Descriptors;

/// <summary>
/// Represents a common descriptor which <see cref="PublisherBound"/> could be configured.
/// </summary>
public class PublisherBoundDescriptor : IBrokerDescriptor
{
    ///<inheritdoc />
    public bool EnableChannelEvents { get; set; }

    /// <summary>
    /// Gets or sets the default message descriptor for this publisher.
    /// </summary>
    public MessageDescriptor Message { get; set; } = new();

    /// <summary>
    /// Gets or sets the configuration for confirms.
    /// </summary>
    public DeliveryConfirmation Confirmation { get; set; } = new();

    /// <summary>
    /// Gets or sets the collection of exchanges used to forward messages.
    /// </summary>
    public List<ExchangeBoundDescriptor> Exchanges { get; set; } = new();

    /// <summary>
    /// 
    /// </summary>
    public Action<IBasicProperties> OnBuildProperties { get; set; } = _ => { };
}
