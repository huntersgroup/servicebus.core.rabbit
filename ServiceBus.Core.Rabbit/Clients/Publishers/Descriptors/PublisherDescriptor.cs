using System;
using RabbitMQ.Client;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Clients.Publishers.Descriptors;

/// <summary>
/// Represents a custom descriptor for basic publishers.
/// </summary>
public class PublisherDescriptor : BrokerDescriptor
{
    /// <summary>
    /// Gets or sets the default message descriptor for this publisher.
    /// </summary>
    public MessageDescriptor Message { get; set; } = new();

    /// <summary>
    /// Gets or sets the exchange descriptor.
    /// </summary>
    public ExchangeDescriptor Exchange { get; set; } = new();

    /// <summary>
    /// Gets or sets the configuration for confirms.
    /// </summary>
    public DeliveryConfirmation Confirmation { get; set; } = new();

    /// <summary>
    /// Gets or sets a custom action for building properties.
    /// </summary>
    public Action<IBasicProperties> OnBuildProperties { get; set; } = _ => { };
}
