using System;
using System.Diagnostics;

namespace ServiceBus.Core.Clients.Publishers.Descriptors;

/// <summary>
/// Describe some features about messages
/// </summary>
[DebuggerDisplay("format: {this.Format}, ttl: {this.Ttl}, persistent: {this.Persistent}")]
public class MessageDescriptor
{
    /// <summary>
    /// Gets or sets the format of incoming messages
    /// </summary>
    public MessageFormat Format { get; set; } = MessageFormat.Header;

    /// <summary>
    /// Gets or sets the default time-to-live for each message sent by publishers.
    /// </summary>
    public TimeSpan? Ttl { get; set; } = TimeSpan.FromMinutes(15);

    /// <summary>
    /// Gets or sets the default value for all messages (if It must be persistent or not)
    /// </summary>
    public bool Persistent { get; set; } = false;
}
