using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using RabbitMQ.Client;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// 
/// </summary>
internal class StatelessRestPublisher : BrokerClient, IRestPublisher
{
    private readonly string exchangeName;
    private readonly RestPublisherDescriptor descriptor;
    private readonly IHttpRestMessageSerializer restMessageSerializer;
    private readonly RestResponse defaultResponse;
    private readonly ConcurrentDictionary<Type, dynamic> dynamicRestResponses;
    private static readonly byte[] EmptyByteArray = Array.Empty<byte>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="dataFormatter"></param>
    /// <param name="restMessageSerializer"></param>
    public StatelessRestPublisher(RestPublisherDescriptor descriptor, IDataFormatter dataFormatter, IHttpRestMessageSerializer restMessageSerializer)
        : base(descriptor)
    {
        this.descriptor = descriptor;
        this.DataFormatter = dataFormatter;
        this.restMessageSerializer = restMessageSerializer;

        var exchangeDescriptor = new ExchangeDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueDescriptor = new QueueDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueBinderDescriptor = new BinderDescriptor();

        this.exchangeName = exchangeDescriptor.GetChannelName();

        // prepares working queue, exchange and working binder for queue flow.
        this.Channel.BuildExchange(exchangeDescriptor)
            .BuildQueue(workingQueueDescriptor)
            .BuildBinder(exchangeDescriptor, workingQueueBinderDescriptor, workingQueueDescriptor);

        this.defaultResponse = new RestResponse
        {
            ResponseStatus = ResponseStatus.Completed,
            StatusCode = HttpStatusCode.OK,
            RawBytes = EmptyByteArray,
            Content = string.Empty
        };

        this.dynamicRestResponses = new ConcurrentDictionary<Type, dynamic>();
    }

    /// <summary>
    /// 
    /// </summary>
    public IDataFormatter DataFormatter { get; }

    /// <inheritdoc/>
    public Task<RestResponse> SendAsync(RestRequest request)
    {
        return Task.Factory.StartNew(() =>
        {
            this.SendRequest(request);
            return defaultResponse;
        });
    }

    /// <inheritdoc/>
    public Task<RestResponse<TData>> SendAsync<TData>(RestRequest request)
    {
        RestResponse<TData> response;

        var type = typeof(TData);

        if (!this.dynamicRestResponses.TryGetValue(type, out var dynResponse))
        {
            response = new RestResponse<TData>
            {
                ResponseStatus = ResponseStatus.Completed,
                StatusCode = HttpStatusCode.OK,
                RawBytes = EmptyByteArray,
                Content = string.Empty,
                Data = default
            };

            this.dynamicRestResponses.TryAdd(type, response);
        }
        else
        {
            response = dynResponse;
        }

        return Task.Factory.StartNew(() =>
        {
            this.SendRequest(request);
            return response;
        });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    private void SendRequest(RestRequest request)
    {
        request.AddOrOverrideHeader(HttpHeaders.ContentType, descriptor.ContextType);

        var timeout = request.Timeout ?? this.descriptor.Timeout ?? TimeSpan.FromSeconds(15);

        var properties = this.Channel.CreateBasicProperties();
        properties.CorrelationId = request.Id;
        properties.Timestamp = new AmqpTimestamp(DateTime.UtcNow.AsUnixDateTime());
        properties.Expiration = timeout.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);

        var httpRequest = request.Translate(this.DataFormatter);
        var payload = this.restMessageSerializer.SerializeRequest(httpRequest);

        this.Channel.BasicPublish(exchange: this.exchangeName, routingKey: string.Empty, basicProperties: properties, body: payload);
    }
}
