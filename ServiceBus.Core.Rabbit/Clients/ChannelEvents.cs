namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
public static class ChannelEvents
{
    /// <summary>
    /// Represents a basic ack command
    /// </summary>
    public const string Acks = "acks";

    /// <summary>
    /// Represents a basic nack command
    /// </summary>
    public const string Nacks = "nacks";

    /// <summary>
    /// Represents a basic return command
    /// </summary>
    public const string Return = "returns";

    /// <summary>
    /// 
    /// </summary>
    public const string FlowControl = "flowControl";

    /// <summary>
    /// Represents any exceptions thrown by channels
    /// </summary>
    public const string CallbackException = "callbackException";

    /// <summary>
    /// Represents raised when the channel is disposed
    /// </summary>
    public const string ModelShutdown = "modelShutdown";
}
