using System;
using System.Threading.Tasks;
using ServiceBus.Core.Clients.Publishers;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Clients;

/// <summary>
/// Represents a common way to forward messages from incoming source (queue) into destination targets (exchanges).
/// </summary>
/// <typeparam name="TData"></typeparam>
public class RoutingBridgeBound<TData> : SubscriberBound<TData>, IRoutingBridgeBound<TData>
{
    private readonly PublisherBound publisher;
    private readonly RoutingBridgeChannels routingBridgeChannels;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="formatter"></param>
    /// <param name="routingBridgeChannels"></param>
    public RoutingBridgeBound(RoutingBridgeDescriptor descriptor, IDataFormatter formatter, RoutingBridgeChannels routingBridgeChannels)
        :base(descriptor.Source, formatter, routingBridgeChannels.Subscriber.Channel)
    {
        this.routingBridgeChannels = routingBridgeChannels;
        this.publisher = new PublisherBound(descriptor.Target, formatter, routingBridgeChannels.Publisher.Channel);

        var nextExecutor = new DelegateHandler<MessageDeliverEventArgs<TData>>();
        nextExecutor.Add(this.HandlerOnAfterEventRaised);

        this.Received = new NextDelegateHandler<MessageDeliverEventArgs<TData>>(nextExecutor, null);
        this.BeforeForwarding = new DelegateHandler<MessageDeliverEventArgs<TData>>();
        this.OnForwardingException = new DelegateHandler<ForwardExceptionEventArg<TData>>();
        this.OnChannelEventFired = new NextDelegateHandler<ChannelEventArgs>(null, new []{ this.publisher.OnChannelEventFired });
    }

    ///<inheritdoc/>
    public IDelegateHandler<MessageDeliverEventArgs<TData>> BeforeForwarding { get; }

    ///<inheritdoc/>
    public IDelegateHandler<ForwardExceptionEventArg<TData>> OnForwardingException { get; }

    ///<inheritdoc/>
    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        this.Received.Clear();
        this.BeforeForwarding.Clear();
        this.OnForwardingException.Clear();
        this.OnChannelEventFired.Clear();
        this.routingBridgeChannels.Dispose();
    }

    /// <summary>
    /// This is the last method executed after executing all message event handlers were executed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async Task HandlerOnAfterEventRaised(object sender, MessageDeliverEventArgs<TData> e)
    {
        await this.BeforeForwarding.InvokeAsync(sender, e);
            
        try
        {
            if (!e.Acknowledged || !this.IsRunning)
            {
                return;
            }

            await this.publisher.SendAsync(e.Message);
        }
        catch (Exception ex)
        {
            // due to the error, it's needed to
            e.Acknowledged = false;

            var forwardEx = new ForwardExceptionEventArg<TData>(e)
            {
                Exception = ex
            };

            await this.OnForwardingException.InvokeAsync(sender, forwardEx);
        }
    }
}
