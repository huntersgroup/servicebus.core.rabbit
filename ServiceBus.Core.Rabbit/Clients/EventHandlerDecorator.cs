using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceBus.Core.Clients;

/// <summary>
/// Represents a way to encapsulate an <see cref="EventHandler{TEvent}"/>, with a future execution of other <see cref="EventHandler{TEvent}"/>, executed after the last <see cref="EventRaised"/> is executed.
/// </summary>
/// <typeparam name="TEvent"></typeparam>
[Obsolete]
public class EventHandlerDecorator<TEvent>
    where TEvent : EventArgs
{
    private event EventHandler<TEvent> EventRaised;
    private event EventHandler<TEvent> AfterEventRaised;
    private readonly List<KeyValuePair<EventHandler<TEvent>, EventHandler<TEvent>>> handlers;

    /// <summary>
    /// 
    /// </summary>
    public EventHandlerDecorator()
    {
        this.handlers = new List<KeyValuePair<EventHandler<TEvent>, EventHandler<TEvent>>>();
    }

    /// <summary>
    /// Gets the main event handler that encapsulates all previous handlers added to the given instance.
    /// </summary>
    public EventHandler<TEvent> EventHandler => this.EventRaised;

    /// <summary>
    /// Removes all delegates of <see cref="EventHandler"/> property.
    /// </summary>
    public void Clear()
    {
        this.EventRaised = null;
    }

    /// <summary>
    /// Calls <see cref="Clear"/> method and removes all delegates from the other event handlers.
    /// </summary>
    public void Reset()
    {
        this.Clear();
        this.AfterEventRaised = null;
    }

    /// <summary>
    /// Adds or remove delegates to the given instance
    /// </summary>
    public event EventHandler<TEvent> OnEventRaised
    {
        add
        {
            if (value == null) return;

            var keyValue = new KeyValuePair<EventHandler<TEvent>, EventHandler<TEvent>>(value,
                (sender, args) => OnEventHandledWithAfter(sender, args, value, this.AfterEventRaised));

            this.handlers.Add(keyValue);
            this.ReevaluateHandlers();
        }
        remove
        {
            if (value == null) return;

            var lastIndex = this.handlers.FindLastIndex(pair => pair.Key == value);

            if (lastIndex <= -1) return;

            this.handlers.RemoveAt(lastIndex);
            this.ReevaluateHandlers();
        }
    }

    /// <summary>
    /// Adds or removes delegates, that they will be executed after all delegates from <see cref="EventRaised"/> property are executed.
    /// </summary>
    public event EventHandler<TEvent> OnAfterEventRaised
    {
        add
        {
            if (value == null) return;

            this.AfterEventRaised += value;
        }
        remove
        {
            if (value == null) return;

            this.AfterEventRaised -= value;
        }
    }

    /// <summary>
    /// Register again all handlers, but the last one will be the handler translated with after delegation execution.
    /// </summary>
    private void ReevaluateHandlers()
    {
        this.Clear();

        if (!this.handlers.Any()) return;

        var lastIndex = this.handlers.Count - 1;

        for (var index = 0; index < lastIndex; index++)
        {
            this.EventRaised += this.handlers[index].Key;
        }

        this.EventRaised += this.handlers[lastIndex].Value;
    }

    /// <summary>
    /// Encapsulates a handler into a new one.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    /// <param name="eventHandler"></param>
    /// <param name="afterEventHandler"></param>
    private static void OnEventHandledWithAfter(object sender, TEvent args, EventHandler<TEvent> eventHandler, EventHandler<TEvent> afterEventHandler)
    {
        eventHandler.Invoke(sender, args);

        afterEventHandler?.Invoke(sender, args);
    }
}
