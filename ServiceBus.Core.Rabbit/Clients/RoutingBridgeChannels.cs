using System;
using RabbitMQ.Client;

namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
public class RoutingBridgeChannels : IDisposable
{
    private readonly ChannelHandler pubChannelHandler;
    private readonly ChannelHandler subChannelHandler;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionFactory"></param>
    public RoutingBridgeChannels(IConnectionFactory connectionFactory)
    {
        this.pubChannelHandler = new ChannelHandler(connectionFactory, "bridge_pub");
        this.subChannelHandler = new ChannelHandler(connectionFactory, "bridge_sub");
    }

    /// <summary>
    /// 
    /// </summary>
    public ChannelHandler Subscriber { get => this.subChannelHandler; }

    /// <summary>
    /// 
    /// </summary>
    public ChannelHandler Publisher { get => this.pubChannelHandler; }

    ///<inheritdoc/>
    public void Dispose()
    {
        this.pubChannelHandler.Dispose();
        this.subChannelHandler.Dispose();
    }
}
