using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using RabbitMQ.Client.Events;

namespace ServiceBus.Core.Clients;

/// <summary>
/// This is a common decorator for <see cref="IConnection"/> instance, which holds closing method on disposing.
/// </summary>
internal class ConnectionDecorator : IConnection
{
    private readonly IConnection decoratee;
    private readonly TimeSpan defaultTimeout;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="decoratee"></param>
    public ConnectionDecorator(IConnection decoratee)
    {
        this.decoratee = decoratee;
        this.defaultTimeout = TimeSpan.FromSeconds(60);
    }

    ///<inheritdoc />
    int INetworkConnection.LocalPort => this.decoratee.LocalPort;

    ///<inheritdoc />
    int INetworkConnection.RemotePort => this.decoratee.RemotePort;

    ///<inheritdoc />
    ushort IConnection.ChannelMax => this.decoratee.ChannelMax;

    ///<inheritdoc />
    IDictionary<string, object> IConnection.ClientProperties => this.decoratee.ClientProperties;

    ///<inheritdoc />
    ShutdownEventArgs IConnection.CloseReason => this.decoratee.CloseReason;
        
    ///<inheritdoc />
    AmqpTcpEndpoint IConnection.Endpoint => this.decoratee.Endpoint;

    ///<inheritdoc />
    uint IConnection.FrameMax => this.decoratee.FrameMax;

    ///<inheritdoc />
    TimeSpan IConnection.Heartbeat => this.decoratee.Heartbeat;

    ///<inheritdoc />
    bool IConnection.IsOpen => this.decoratee.IsOpen;

    ///<inheritdoc />
    AmqpTcpEndpoint[] IConnection.KnownHosts => this.decoratee.KnownHosts;

    ///<inheritdoc />
    IProtocol IConnection.Protocol => this.decoratee.Protocol;

    ///<inheritdoc />
    IDictionary<string, object> IConnection.ServerProperties => this.decoratee.ServerProperties;

    ///<inheritdoc />
    IList<ShutdownReportEntry> IConnection.ShutdownReport => this.decoratee.ShutdownReport;

    ///<inheritdoc />
    string IConnection.ClientProvidedName => this.decoratee.ClientProvidedName;

    ///<inheritdoc />
    event EventHandler<CallbackExceptionEventArgs> IConnection.CallbackException
    {
        add => this.decoratee.CallbackException += value;
        remove => this.decoratee.CallbackException -= value;
    }

    ///<inheritdoc />
    event EventHandler<ConnectionBlockedEventArgs> IConnection.ConnectionBlocked
    {
        add => this.decoratee.ConnectionBlocked += value;
        remove => this.decoratee.ConnectionBlocked -= value;
    }

    ///<inheritdoc />
    event EventHandler<ShutdownEventArgs> IConnection.ConnectionShutdown
    {
        add => this.decoratee.ConnectionShutdown += value;
        remove => this.decoratee.ConnectionShutdown -= value;
    }

    ///<inheritdoc />
    event EventHandler<EventArgs> IConnection.ConnectionUnblocked
    {
        add => this.decoratee.ConnectionUnblocked += value;
        remove => this.decoratee.ConnectionUnblocked -= value;
    }

    ///<inheritdoc />
    void IDisposable.Dispose()
    {
        IConnection current = this;
        current.Close();

        this.decoratee.Dispose();
    }

    ///<inheritdoc />
    void IConnection.UpdateSecret(string newSecret, string reason)
    {
        this.decoratee.UpdateSecret(newSecret, reason);
    }

    ///<inheritdoc />
    void IConnection.Abort()
    {
        this.decoratee.Abort();
    }

    ///<inheritdoc />
    void IConnection.Abort(ushort reasonCode, string reasonText)
    {
        this.decoratee.Abort(reasonCode, reasonText);
    }

    ///<inheritdoc />
    void IConnection.Abort(TimeSpan timeout)
    {
        this.decoratee.Abort(timeout);
    }

    ///<inheritdoc />
    void IConnection.Abort(ushort reasonCode, string reasonText, TimeSpan timeout)
    {
        this.decoratee.Abort(reasonCode, reasonText, timeout);
    }

    ///<inheritdoc />
    void IConnection.Close()
    {
        if (this.decoratee.IsOpen)
        {
            this.decoratee.Close(this.defaultTimeout);
        }
    }

    ///<inheritdoc />
    void IConnection.Close(ushort reasonCode, string reasonText)
    {
        if (this.decoratee.IsOpen)
        {
            this.decoratee.Close(reasonCode, reasonText);
        }
    }

    ///<inheritdoc />
    void IConnection.Close(TimeSpan timeout)
    {
        if (this.decoratee.IsOpen)
        {
            this.decoratee.Close(timeout);
        }
    }

    ///<inheritdoc />
    void IConnection.Close(ushort reasonCode, string reasonText, TimeSpan timeout)
    {
        if (this.decoratee.IsOpen)
        {
            this.decoratee.Close(reasonCode, reasonText, timeout);
        }
    }

    ///<inheritdoc />
    IModel IConnection.CreateModel()
    {
        var wrapped = this.decoratee.CreateModel();
        return new ModelDecorator(wrapped);
    }

    ///<inheritdoc />
    void IConnection.HandleConnectionBlocked(string reason)
    {
        this.decoratee.HandleConnectionBlocked(reason);
    }

    ///<inheritdoc />
    void IConnection.HandleConnectionUnblocked()
    {
        this.decoratee.HandleConnectionUnblocked();
    }
}
