using System;
using Microsoft.Extensions.Configuration;

namespace ServiceBus.Demo.Publisher.Extensions;

internal static class CommonExtensions
{
    internal static string GetExecutionEnv(this Application app)
    {
        return Environment.GetEnvironmentVariable("NETCOREAPP_ENVIRONMENT");
    }

    internal static IConfigurationRoot BuildConfiguration(this Application app)
    {
        var env = app.GetExecutionEnv();

        var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env}.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables();

        var configuration = builder.Build();

        return configuration;
    }
}
