using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Publishers;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.IO;
using ServiceBus.Core.Model;
using ServiceBus.Demo.Model;
using ServiceBus.Demo.Publisher.Config;
using ServiceBus.Demo.Publisher.Extensions;
using Newtonsoft.Json.Converters;
using ServiceBus.Core.Formatters.Extensions;

namespace ServiceBus.Demo.Publisher;

class Application
{
    private readonly IAsyncConnectionFactory connectionFactory;
    private  IDataFormatter dataFormatter;
    private string apiKey;

    public Application()
    {
        this.Configuration = this.BuildConfiguration();
        this.AppSettings = this.Configuration.Get<AppSettings>();
        this.connectionFactory = new ConnectionFactory
        {
            Uri = this.AppSettings.EsbAddress,
            UserName = this.AppSettings.EsbCredentials.Username,
            Password = this.AppSettings.EsbCredentials.Password
        };
    }

    public IConfigurationRoot Configuration { get; }

    public AppSettings AppSettings { get; }

    private static async Task Main(string[] args)
    {
        IDataFormatter dataFormatter;

        Console.WriteLine("## Please chose the formatter mode to use:");
        Console.WriteLine("## Press 1 for Json.net");
        Console.WriteLine("## otherwise any key Microsoft Text json");

        var formatterCode = Console.ReadLine();

        switch (formatterCode)
        {
            case "1":
            {
                var jsonSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    FloatParseHandling = FloatParseHandling.Decimal,
                    NullValueHandling = NullValueHandling.Ignore
                };

                jsonSettings.Converters.Add(new StringEnumConverter());

                dataFormatter = new JsonDataFormatter(jsonSettings, Encoding.UTF8);

                break;
            }
            default:
            {
                var options = new JsonSerializerOptions
                {
                    DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
                    PropertyNameCaseInsensitive = true,
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    ReferenceHandler = ReferenceHandler.IgnoreCycles,
                    DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
                };

                options.Converters.Add(new JsonStringEnumConverter());
                options.ApplyRealNumberWithFractionalPortion();

                dataFormatter = new MsJsonDataFormatter(options);

                break;
            }
        }

        var app = new Application
        {
            apiKey = Guid.NewGuid().ToString("D"),
            dataFormatter = dataFormatter
        };

        Console.WriteLine("### Choose the demo to execute: ###");
        Console.WriteLine();
        Console.WriteLine($"Press 1 for {nameof(ExecuteDirectPublisher)}");
        Console.WriteLine($"Press 2 for {nameof(ExecuteDirectBinderPublisher)}");
        Console.WriteLine($"Press 3 for {nameof(ExecuteDirectPublisherWithRouting)}");
        Console.WriteLine($"Press 4 for {nameof(ExecuteFanoutPublisher)}");
        Console.WriteLine($"Press 5 for {nameof(ExecuteTopicPublisher)}");
        Console.WriteLine($"Press 6 for {nameof(ExecuteTopicPublisherNoRouting)}");
        Console.WriteLine($"Press 7 for {nameof(ExecuteDirectPublisherWithHttpRequest)}");
        Console.WriteLine($"Press 8 for {nameof(ExecuteFakeRequest)}");
        Console.WriteLine($"Press 9 for {nameof(ExecuteRestPublisher)}");
        Console.WriteLine($"Press 10 for {nameof(ExecuteRestPublisherPressureTest)}");
        Console.WriteLine($"Press 11 for {nameof(ExecuteManyDirectPublisher)}, similar to 1");
        Console.WriteLine($"Press 12 for {nameof(SendPersonUsingDirectPublisher)} similar to 1");
        Console.WriteLine($"Press 13 for {nameof(ExecutePublisherBound01)}");
        Console.WriteLine($"Press 14 for {nameof(ExecutePublisherBoundWithManyMessages)}");
        Console.WriteLine($"Press 15 for {nameof(ExecutePublisherForTempConsumers)}");

        var code = Console.ReadLine();

        switch (code)
        {
            case "1":
            {
                Console.Write("write the exchange name (empty for default): ");
                var exchange = Console.ReadLine();

                Console.WriteLine();

                await app.ExecuteDirectPublisher(string.IsNullOrWhiteSpace(exchange) ? "demo-direct" : exchange);
                break;
            }
            case "2":
            {
                Console.Write("write the exchange name (empty for default): ");
                var exchange = Console.ReadLine();

                Console.WriteLine();

                Console.Write("write the queue name (empty for default): ");
                var queue = Console.ReadLine();

                Console.WriteLine();
                await app.ExecuteDirectBinderPublisher(string.IsNullOrWhiteSpace(exchange) ? "demo-direct" : exchange, string.IsNullOrWhiteSpace(queue) ? "demo" : queue);
                break;
            }
            case "3":
            {
                Console.Write("write the routing key: ");
                var routingKey = Console.ReadLine();

                Console.WriteLine();

                await app.ExecuteDirectPublisherWithRouting(routingKey);
                break;
            }
            case "4":
            {
                await app.ExecuteFanoutPublisher();
                break;
            }
            case "5":
            {
                Console.Write("write the routing key: ");
                var routingKey = Console.ReadLine();

                Console.WriteLine();

                await app.ExecuteTopicPublisher(routingKey);
                break;
            }
            case "6":
            {
                await app.ExecuteTopicPublisherNoRouting();
                break;
            }
            case "7":
            {
                await app.ExecuteDirectPublisherWithHttpRequest(null);
                break;
            }
            case "8":
            {
                await app.ExecuteFakeRequest();
                break;
            }
            case "9":
            {
                Console.Write("Do you use a full duplex channel?: ");
                var flag = Convert.ToBoolean(Console.ReadLine());

                await app.ExecuteRestPublisher(flag);
                break;
            }
            case "10":
            {
                Console.Write("write the number of message to send: ");
                var counter = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine();

                await app.ExecuteRestPublisherPressureTest(counter);
                break;
            }
            case "11":
            {
                Console.Write("write the number of message to send: ");
                var counter = Convert.ToInt32(Console.ReadLine());

                await app.ExecuteManyDirectPublisher(counter);
                break;
            }
            case "12":
            {
                await app.SendPersonUsingDirectPublisher();
                break;
            }
            case "13":
            {
                await app.ExecutePublisherBound01();
                break;
            }
            case "14":
            {
                Console.Write("write the number of message to send: ");
                var counter = Convert.ToInt32(Console.ReadLine());

                await app.ExecutePublisherBoundWithManyMessages(counter);
                break;
            }
            case "15":
            {
                await app.ExecutePublisherForTempConsumers();
                break;
            }
            default:
            {
                Console.WriteLine("No option available !!!");
                break;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="exchange">Queue name to use for publisher</param>
    /// <returns></returns>
    private async Task ExecuteDirectPublisher(string exchange)
    {
        var descriptor = new PublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = exchange, Type = ExchangeTypes.Direct }
        };
            
        var publisher = new CommonPublisher(descriptor, this.dataFormatter);

        this.OnPublishing(publisher);

        await Task.FromResult(0);
    }
        
    private async Task ExecuteManyDirectPublisher2(int count)
    {
        var list = new List<Task>();

        var descriptor = new BinderPublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "demo-direct", Type = ExchangeTypes.Direct },
            Queue = { Name = "demo" }
            //Queue = { Name = "demo", AutoDelete = false, Durable = true }
            //Queue = { Name = "demo", Durable = true, AutoDelete = false, Arguments = { { "x-queue-type", "quorum" } } }
        };

        using (var publisher = new BinderPublisher(descriptor, this.dataFormatter))
        {
            for (int i = 0; i < count; i++)
            {
                var message = $"message index: {i}, body: {Guid.NewGuid():N}";
                var tks = publisher.SendAsync(message);

                await tks;

                list.Add(tks);
            }

            Console.WriteLine("All tasks were executed...");
        }

        //Task.WaitAll(list.ToArray());

        Console.WriteLine("all end");
        Console.ReadLine();

        await Task.FromResult(0);
    }

    private async Task ExecuteManyDirectPublisher(int count)
    {
        var watch = new Stopwatch();

        var descriptor = new BinderPublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            EnableChannelEvents = true,
            Exchange = { Name = "demo-direct", Type = ExchangeTypes.Direct },
            //Queue = { Name = "demo" }
            Queue = { Name = "demo", AutoDelete = false, Durable = true }
            //Queue = { Name = "demo", Durable = true, AutoDelete = false, Arguments = { { "x-queue-type", "quorum" } } }
            , Confirmation =
            {
                Enabled = true,
                Timeout = TimeSpan.FromMinutes(1),
                WaitFor = true
            }
        };

        watch.Start();

        using (var publisher = new BinderPublisher(descriptor, this.dataFormatter))
        {
            publisher.OnChannelEventFired += (sender, args) =>
            {
                switch (args.EventTag)
                {
                    case ChannelEvents.Acks:
                    {
                        var properties = $"properties[deliveryTag: {args.Properties[nameof(BasicAckEventArgs.DeliveryTag)]}, multiple: {args.Properties[nameof(BasicAckEventArgs.Multiple)]}]";
                        Console.WriteLine($"event raised: {args.EventTag}, {properties}");
                        break;
                    }
                    case ChannelEvents.Nacks:
                    {
                        var properties = $"properties[deliveryTag: {args.Properties[nameof(BasicNackEventArgs.DeliveryTag)]}, multiple: {args.Properties[nameof(BasicNackEventArgs.Multiple)]}]";
                        Console.WriteLine($"event raised: {args.EventTag}, {properties}");
                        break;
                    }
                    default:
                    {
                        Console.WriteLine($"event raised: {args.EventTag}");
                        break;
                    }
                }
            };

            for (var i = 0; i < count; i++)
            {
                var message = new Message<dynamic>
                {
                    Data = $"message index: {i}, body: {Guid.NewGuid():N}",
                    Persistent = true
                };

                //await publisher.SendAsync(message)
                //    .ContinueWith(task =>
                //    {
                //        Console.WriteLine($"#### ContinueWith, tag: {message.Tag}");
                //    });

                // OR

                await publisher.SendAsync(message);
                //Console.WriteLine($"#### ContinueWith, tag: {message.Tag}");

                //// this is wrong, due to without await the message.Tag is not setup yet !!!
                //var task = publisher.SendAsync(message);
                //Console.WriteLine($"#### ContinueWith, tag: {message.Tag}");
                //await task;
            }

            Console.WriteLine();
            Console.WriteLine("All tasks were executed...");

            Console.WriteLine("waiting for ending tasks...");
        }

        watch.Stop();
        var time = watch.Elapsed.TotalSeconds;

        Console.WriteLine($"all end, total seconds: {time}");

        await Task.FromResult(0);
    }

    private async Task ExecuteDirectBinderPublisher(string exchange, string queue)
    {
        var descriptor = new BinderPublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = exchange, Type = ExchangeTypes.Direct },
            Queue = { Name = queue }
            //Queue = { Name = queue, AutoDelete = false, Durable = true, Arguments =
            //{
            //    {"x-queue-type", "quorum"}
            //}}
            //Queue = { Name = queue, AutoDelete = false, Durable = true, Arguments =
            //{
            //    {"x-queue-type", "stream"},
            //    //{"x-max-age", "10m"},
            //    {"x-stream-max-segment-size-bytes", 1000_000}
            //}}
        };

        var publisher = new BinderPublisher(descriptor, this.dataFormatter);

        this.OnPublishing(publisher);

        await Task.FromResult(0);
    }

    private async Task ExecuteDirectPublisherWithRouting(string routingKey)
    {
        var descriptor = new PublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "demo-direct-route", Type = ExchangeTypes.Direct }
        };

        var publisher = new CommonPublisher(descriptor, this.dataFormatter);

        this.OnPublishing(publisher, routingKey);

        await Task.FromResult(0);
    }

    private async Task ExecuteFanoutPublisher()
    {
        var descriptor = new PublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "demo-broadcast", Type = ExchangeTypes.Fanout }
        };

        var publisher = new CommonPublisher(descriptor, this.dataFormatter);

        this.OnPublishing(publisher);

        await Task.FromResult(0);
    }

    private async Task ExecuteTopicPublisher(string routingKey)
    {
        var descriptor = new PublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "demo-topic-route", Type = ExchangeTypes.Topic }
        };

        var publisher = new CommonPublisher(descriptor, this.dataFormatter);

        this.OnPublishing(publisher, routingKey);

        await Task.FromResult(0);
    }

    private async Task ExecuteTopicPublisherNoRouting()
    {
        var descriptor = new PublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "demo-topic", Type = ExchangeTypes.Topic, Durable = true, AutoDelete = false }
        };

        var publisher = new CommonPublisher(descriptor, this.dataFormatter);

        this.OnPublishing(publisher);

        await Task.FromResult(0);
    }

    private async Task ExecuteDirectPublisherWithHttpRequest(string routingKey)
    {
        var descriptor = new BinderPublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "demo-direct", Type = ExchangeTypes.Direct },
            Queue = { Name = "demo" }
        };

        Console.WriteLine($"##### Starting producer, apiKey: {this.apiKey} #####");
        Console.WriteLine(" Code [exit] to exit.");
            
        using (var publisher = new BinderPublisher(descriptor, this.dataFormatter))
        {
            while (true)
            {
                var request = new HttpRestRequest();

                try
                {
                    Console.WriteLine("Put HttpRestRequest properties, write exit to escape!");
                    Console.Write($"{nameof(HttpRestRequest.Version)}: ");
                    
                    request.Version = ReadLine();

                    Console.Write($"{nameof(HttpRestRequest.Method)}: ");
                    request.Method = ReadLine().ToHttpVerb();

                    Console.Write($"{nameof(HttpRestRequest.Resource)}: ");
                    request.Resource = ReadLine();

                    // headers e content cannot be added from commandline
                    request.Content = this.dataFormatter.Encode(new
                    {
                        Id = 100,
                        Value = "current value",
                        Price = 100.56
                    });

                    var message = new Message<HttpRestRequest>
                    {
                        Route = routingKey,
                        Data = request,
                        Properties = { { nameof(this.apiKey), this.apiKey } }
                    };

                    publisher.Send(message);

                    Console.WriteLine("Message sent !!!");
                    Console.WriteLine();
                }
                catch (Exception)
                {
                    Console.WriteLine("Byez !!!");
                    break;
                }
            }
        }

        await Task.FromResult(0);
    }

    private async Task ExecuteFakeRequest()
    {
        var descriptor = new PublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "demo-direct", Type = ExchangeTypes.Direct }
        };

        var publisher = new CommonPublisher(descriptor, dataFormatter);

        this.OnPublishing(publisher, "callbackqueue");

        await Task.FromResult(0);
    }
        
    private async Task ExecuteRestPublisher(bool fullDuplex)
    {
        var descriptor = new RestPublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            ServiceName = "MyAppDemo",
            Timeout = TimeSpan.FromMinutes(1),
            IncludeContent = true,
            Durable = true,
            FullDuplex = fullDuplex
        };

        var restHttpSerializer = new HttpRestMessageSerializer(new FuncMemoryStreamResolver(() => new MemoryStream(), bytes => new MemoryStream(bytes)), Encoding.UTF8);

        using (var client = new RestPublisher(descriptor, this.dataFormatter, restHttpSerializer))
        {
            Console.WriteLine("**** Code [exit] to exit. ****");

            Console.WriteLine("Write your service request, payload is a json object (write \"exit\" to escape)");
            Console.WriteLine();

            while (true)
            {
                try
                {
                    Console.Write($"{nameof(RestRequest.Resource)}:\t");

                    var request = new RestRequest(ReadLine());

                    Console.Write($"{nameof(RestRequest.Method)}:\t\t");
                    var readLine = ReadLine();
                    request.Method = Enum.Parse<HttpMethods>(string.IsNullOrWhiteSpace(readLine) ? "GET" : readLine,
                        true);

                    Console.Write($"{nameof(RestRequest.Payload)}:\t");
                    request.Payload = JsonConvert.DeserializeObject(ReadLine());

                    var resp = await client.SendAsync<object>(request);

                    Console.WriteLine(
                        $"Response:\t {nameof(resp.IsSuccessful)}: {resp.IsSuccessful}, {nameof(resp.StatusCode)}: {resp.StatusCode}, {nameof(resp.Content)}: {resp.Content}");

                    Console.WriteLine(
                        $"Response.Body:\t {JsonConvert.SerializeObject(resp.Data)}");

                    Console.WriteLine();
                }
                catch (OperationCanceledException e)
                {
                    Console.WriteLine($"Execution is over: {e.Message}");
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Something was happened: {e.Message}");
                    Console.WriteLine();
                }
            }
        }

        await Task.FromResult(0);
    }
        
    private async Task ExecuteRestPublisherPressureTest(int counter)
    {
        var descriptor = new RestPublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            ServiceName = "MyAppDemo",
            Timeout = TimeSpan.FromMinutes(30),
            IncludeContent = true,
            Durable = true
        };

        var restHttpSerializer = new HttpRestMessageSerializer(new FuncMemoryStreamResolver(() => new MemoryStream(), bytes => new MemoryStream(bytes)), Encoding.UTF8);

        var watch = new Stopwatch();
            
        using (var client = new RestPublisher(descriptor, this.dataFormatter, restHttpSerializer))
        {
            Console.WriteLine("**** Code [exit] to exit. ****");
            Console.WriteLine();

            var apiKey = Guid.NewGuid().ToString("N");

            var list = new List<Task>();

            watch.Start();

            for (var i = 1; i <= counter; i++)
            {
                var request = new RestRequest("api/demo")
                {
                    Method = HttpMethods.Post,
                    Payload = new { Id = i, Value = $"Custom value {i}", Price = i * 1.5 }
                };

                var tsk = client.SendAsync<object>(request)
                    .ContinueWith(task =>
                    {
                        try
                        {
                            var resp = task.Result;

                            Console.WriteLine(
                                $"Response:\t apiKey: {apiKey}, {nameof(resp.IsSuccessful)}: {resp.IsSuccessful}, {nameof(resp.StatusCode)}: {resp.StatusCode}, {nameof(resp.Data)}: {JsonConvert.SerializeObject(resp.Data)}");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"Exception: {e.Message}");
                        }
                    });

                list.Add(tsk);
            }

            Console.WriteLine("Waiting for all tasks completed !!!");
            Task.WaitAll(list.ToArray());

            watch.Stop();
        }
            
        Console.WriteLine($"Write any keyword to exit, milliseconds elapsed: {watch.Elapsed.TotalMilliseconds}");
        Console.ReadLine();

        await Task.FromResult(0);
    }

    private async Task SendPersonUsingDirectPublisher()
    {
        var descriptor = new PublisherDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Exchange = { Name = "person-direct", Type = ExchangeTypes.Direct }
        };

        var publisher = new CommonPublisher(descriptor, this.dataFormatter);

        this.OnPublishingPerson(publisher);

        await Task.FromResult(0);
    }

    private void OnPublishingPerson(CommonPublisher publisher, string route = "")
    {
        Console.WriteLine($"##### Starting producer, apikey: {this.apiKey} #####");
        Console.WriteLine(" Code [exit] to exit, otherwise write the name of person.");
        var exit = "exit";

        var rdn = new Random();

        using (publisher)
        {
            while (true)
            {
                var message = Console.ReadLine();

                if (exit.Equals(message))
                {
                    break;
                }

                var p = new Person
                {
                    Name = message,
                    Surname = $"surname of {message}",
                    YearBorn = rdn.Next(1980, 1990),
                    Children =
                    [
                        new Person { Name = message, Surname = $"surname of {message}", YearBorn = rdn.Next(1991, 2020) },
                        new Person { Name = message, Surname = $"surname of {message}", YearBorn = rdn.Next(1991, 2020) },
                        new Person { Name = message, Surname = $"surname of {message}", YearBorn = rdn.Next(1991, 2020) }
                    ]
                };

                var instance = new Message<Collection<Person>>
                {
                    Data = [p],
                    Route = route,
                    Ttl = TimeSpan.FromMinutes(3),
                    Properties = { { nameof(this.apiKey), this.apiKey } }
                };

                publisher.Send(instance);
            }
        }
    }

    private async Task ExecutePublisherBound01()
    {
        var descriptor = new PublisherBoundDescriptor
        {
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "demo_01", Type = ExchangeTypes.Direct, Durable = true,
                    Queues =
                    {
                        new QueueBoundDescriptor
                        {
                            Name = "demo_01_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "quorum"}
                            }
                        },
                        new QueueBoundDescriptor
                        {
                            Name = "demo_02_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "classic"}
                            }
                        }
                    },
                    Exchanges =
                    {
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_01", Type = ExchangeTypes.Direct, Durable = true,
                            Queues =
                            {
                                new QueueBoundDescriptor
                                {
                                    Name = "demo_01_01_q", Durable = true, AutoDelete = false, Arguments =
                                    {
                                        {"x-queue-type", "stream"}
                                    }
                                }
                            }
                        },
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_02", Type = ExchangeTypes.Direct, Durable = true
                        }
                    }
                }
            }
        };

        //var a = new ConnectionFactory();
        using (var connection = this.connectionFactory.BuildConnection(factory => factory.CreateConnection("demo-pub")))
        {
            using (var channel = connection.CreateModel())
            {
                var publisher = new PublisherBound(descriptor, this.dataFormatter, channel);
                this.OnPublishing(publisher);
            }
        }
            
        await Task.FromResult(0);
    }

    private async Task ExecutePublisherBoundWithManyMessages(int messageCounter)
    {
        var descriptor = new PublisherBoundDescriptor
        {
            Message =
            {
                Ttl = TimeSpan.FromHours(2)
            },
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "demo_01", Type = ExchangeTypes.Direct, Durable = true,
                    Queues =
                    {
                        new QueueBoundDescriptor
                        {
                            Name = "demo_01_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "quorum"}
                            }
                        },
                        new QueueBoundDescriptor
                        {
                            Name = "demo_02_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "classic"}
                            }
                        }
                    },
                    Exchanges =
                    {
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_01", Type = ExchangeTypes.Direct, Durable = true,
                            Queues =
                            {
                                new QueueBoundDescriptor
                                {
                                    Name = "demo_01_01_q", Durable = true, AutoDelete = false, Arguments =
                                    {
                                        {"x-queue-type", "stream"}
                                    }
                                }
                            }
                        },
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_02", Type = ExchangeTypes.Direct, Durable = true
                        }
                    }
                }
            }
        };

        using (var connection = this.connectionFactory.BuildConnection(factory => factory.CreateConnection("demo-pub")))
        {
            using (var channel = connection.CreateModel())
            {
                using (var publisher = new PublisherBound(descriptor, this.dataFormatter, channel))
                {
                    Console.WriteLine($"##### Starting producer, apikey: {this.apiKey} #####");

                    var dd = new DateTime(1980, 1, 10, 0, 10, 0);
                    var dec = 1.15m;
                    var rating = 12.59;

                    for (var i = 0; i < messageCounter; i++)
                    {
                        var instance = new ComplexCls
                        {
                            Id = i,
                            Name = $"name {i}",
                            Type = ClsType.Complex,
                            BirthDate = dd.AddDays(i),
                            Description = $"description {i}",
                            DecVal = dec + i,
                            DoubleVal = rating,
                            Alias = new[] { $"alias {i}", $"alias {i + 1}" },
                            Parent = new ComplexCls { Id = 1, Name = $"parent {i}", Type = ClsType.Common },
                            Children = new[]
                            {
                                new ComplexCls{ Id = i, Name = $"child 1", DecVal = 0m, DoubleVal = 0d},
                                new ComplexCls{ Id = i, Name = $"child 2", DecVal = 0m, DoubleVal = 0d}
                            },
                            Tags = new Dictionary<string, string>
                            {
                                {"tag0", "tag 1"},
                                {"tag1", "tag 2"},
                                {"tag3", "tag 3"}
                            },
                            Fields = new Dictionary<string, object>
                            {
                                {"intVal", 0},
                                {"longVal", 0l},
                                {"doubleVal", 0.25},
                                {"decVal", 0.78m},
                                {"doubleVal_1", 0.0},
                                {"decVal_1", 0.0m},
                                {"par3", new DateTime(2020, 10, 10)},
                            }
                        };

                        object message = new Message<ComplexCls>
                        {
                            Data = instance
                        };

                        await publisher.SendAsync(message);
                    }
                }
            }
        }

        Console.WriteLine("press any key to close");
        Console.ReadLine();
        //await Task.FromResult(0);
    }


    private async Task ExecutePublisherForTempConsumers()
    {
        var descriptor = new PublisherBoundDescriptor
        {
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "temp", Durable = true, Type = ExchangeTypes.Direct
                }
            }
        };

        using (var connection = this.connectionFactory.CreateConnection("pub-4-temp"))
        {
            using (var channel = connection.CreateModel())
            {
                var publisher = new PublisherBound(descriptor, this.dataFormatter, channel);

                Console.WriteLine($"##### Starting producer, apikey: {this.apiKey} #####");
                Console.WriteLine(" Code [exit] to exit.");
                const string exit = "exit";

                while (true)
                {
                    Console.Write("write message: ");
                    var message = Console.ReadLine();

                    Console.Write("write routing: ");
                    var route = Console.ReadLine();

                    if (exit.Equals(message) || exit.Equals(route))
                    {
                        break;
                    }

                    var instance = new Message<string>
                    {
                        Data = message,
                        Route = route,
                        Ttl = TimeSpan.FromMinutes(5),
                        Properties = { { nameof(this.apiKey), this.apiKey } }
                    };

                    await publisher.SendAsync(instance);
                }
            }
        }

        await Task.FromResult(0);
    }

    private void OnPublishing(IPublisher publisher, string route = "")
    {
        Console.WriteLine($"##### Starting producer, apikey: {this.apiKey} #####");
        Console.WriteLine(" Code [exit] to exit.");
        var exit = "exit";

        while (true)
        {
            var message = Console.ReadLine();

            if (exit.Equals(message))
            {
                break;
            }

            var instance = new Message<dynamic>
            {
                //Data = new { id = 10, value=message },
                Data = message,
                Route = route,
                Ttl = TimeSpan.FromMinutes(20),
                Properties = { { nameof(this.apiKey), this.apiKey } }
            };

            publisher.Send(instance);
        }
    }

    private void OnPublishing(IAsyncPublisher publisher, string route = "")
    {
        Console.WriteLine($"##### Starting producer, apikey: {this.apiKey} #####");
        Console.WriteLine(" Code [exit] to exit.");
        var exit = "exit";

        while (true)
        {
            var message = Console.ReadLine();

            if (exit.Equals(message))
            {
                break;
            }

            var instance = new Message<dynamic>
            {
                //Data = new { id = 10, value=message },
                Data = message,
                Route = route,
                Ttl = TimeSpan.FromMinutes(20),
                Properties = { { nameof(this.apiKey), this.apiKey } }
            };

            publisher.Send(instance);
        }
    }

    //private void OnPublishing(CommonPublisher publisher, string route = "")
    //{
    //    Console.WriteLine($"##### Starting producer, apikey: {this.apiKey} #####");
    //    Console.WriteLine(" Code [exit] to exit.");
    //    var exit = "exit";

    //    using (publisher)
    //    {
    //        while (true)
    //        {
    //            var message = Console.ReadLine();

    //            if (exit.Equals(message))
    //            {
    //                break;
    //            }

    //            var instance = new Message<dynamic>
    //            {
    //                //Data = new { id = 10, value=message },
    //                Data = message,
    //                Route = route,
    //                Ttl = TimeSpan.FromMinutes(10),
    //                Properties = { {nameof(this.apiKey), this.apiKey} }
    //            };

    //            publisher.Send(instance);
    //        }
    //    }
    //}

    private static string ReadLine()
    {
        var exit = "exit";

        var line = Console.ReadLine();

        if (line != null && line.Equals(exit, StringComparison.OrdinalIgnoreCase))
        {
            throw new OperationCanceledException("Exit command was sent.");
        }

        return line;
    }
}
