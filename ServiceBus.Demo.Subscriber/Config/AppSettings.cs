using System;
using ServiceBus.Core.Clients;

namespace ServiceBus.Demo.Subscriber.Config;

public class AppSettings
{
    public Uri EsbAddress { get; set; }

    public Credentials EsbCredentials { get; set; }
}
