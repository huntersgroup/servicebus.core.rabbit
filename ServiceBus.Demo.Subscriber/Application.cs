using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using ServiceBus.Core.Clients.Consumers;
using ServiceBus.Core.Clients.Publishers;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Model;
using ServiceBus.Demo.Model;
using ServiceBus.Demo.Subscriber.Config;
using ServiceBus.Demo.Subscriber.Extensions;

namespace ServiceBus.Demo.Subscriber;

internal class Application
{
    private readonly IDataFormatter dataFormatter;
    private readonly IAsyncConnectionFactory connectionFactory;
    private readonly IAsyncConnectionFactory asyncConnectionFactory;
    private string apiKey;

    public Application()
    {
        var jsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        this.dataFormatter = new JsonDataFormatter(jsonSettings, Encoding.UTF8);
        this.Configuration = this.BuildConfiguration();
        this.AppSettings = this.Configuration.Get<AppSettings>();
        this.connectionFactory = new ConnectionFactory
        {
            Uri = this.AppSettings.EsbAddress,
            UserName = this.AppSettings.EsbCredentials.Username,
            Password = this.AppSettings.EsbCredentials.Password
        };

        this.asyncConnectionFactory = new ConnectionFactory
        {
            Uri = this.AppSettings.EsbAddress,
            UserName = this.AppSettings.EsbCredentials.Username,
            Password = this.AppSettings.EsbCredentials.Password,
            DispatchConsumersAsync = true
        };
    }

    public IConfigurationRoot Configuration { get; }

    public AppSettings AppSettings { get; }

    // ReSharper disable once UnusedParameter.Local
    private static async Task Main(string[] args)
    {
        var app = new Application
        {
            apiKey = Guid.NewGuid().ToString("D")
        };

        Console.WriteLine("### Choose the demo to execute: ###");
        Console.WriteLine();
        Console.WriteLine($"Press 1 for {nameof(ExecuteDirectSubscriber)}");
        Console.WriteLine($"Press 2 for {nameof(ExecuteDirectBinderSubscriber)}");
        Console.WriteLine($"Press 3 for {nameof(ExecuteDirectBinderSubscriberWithRouting)}");
        Console.WriteLine($"Press 4 for {nameof(ExecuteFanoutBinderSubscriber)}");
        Console.WriteLine($"Press 5 for {nameof(ExecuteTopicBinderSubscriber)}");
        Console.WriteLine($"Press 6 for {nameof(ExecuteDirectSubscriberWithHttpRequest)}");
        Console.WriteLine($"Press 7 for {nameof(ExecuteDirectSubscriberFake)}");
        Console.WriteLine($"Press 8 for {nameof(ExecuteDirectSubscriberRequeue)}");
        Console.WriteLine($"Press 9 for {nameof(ExecuteBinderSubscriberRequeue)}");
        Console.WriteLine($"Press 10 for {nameof(ReceivePersonUsingDirectBinderSubscriber)}");
        Console.WriteLine($"Press 11 for {nameof(ExecuteConsumer)}");
        Console.WriteLine($"Press 12 for {nameof(ExecuteSubscriberBound01)}");
        Console.WriteLine($"Press 13 for {nameof(MultipleSubscriberBound01)}");
        Console.WriteLine($"Press 15 for {nameof(ExecuteBoundConsumer)}");
        Console.WriteLine($"Press 16 for: {nameof(ExecuteTempSubscriber)}");

        var code = Console.ReadLine();

        switch (code)
        {
            case "1":
            {
                await app.ExecuteDirectSubscriber();
                break;
            }
            case "2":
            {
                await app.ExecuteDirectBinderSubscriber();
                break;
            }
            case "3":
            {
                Console.Write("write the routing key: ");
                var routingKey = Console.ReadLine();

                Console.WriteLine();
                Console.Write("write the queue name: ");

                var queue = Console.ReadLine();

                await app.ExecuteDirectBinderSubscriberWithRouting(routingKey, queue);
                break;
            }
            case "4":
            {
                Console.Write("write the queue name: ");

                var queue = Console.ReadLine();
                Console.WriteLine();

                await app.ExecuteFanoutBinderSubscriber(queue);
                break;
            }
            case "5":
            {
                Console.Write("write the route contract key: ");
                var routeContract = Console.ReadLine();

                Console.WriteLine();
                Console.Write("write the queue name: ");

                var queue = Console.ReadLine();

                await app.ExecuteTopicBinderSubscriber(routeContract, queue);
                break;
            }
            case "6":
            {
                await app.ExecuteDirectSubscriberWithHttpRequest();
                break;
            }
            case "7":
            {
                await app.ExecuteDirectSubscriberFake();
                break;
            }
            case "8":
            {
                await app.ExecuteDirectSubscriberRequeue();
                break;
            }
            case "9":
            {
                await app.ExecuteBinderSubscriberRequeue();
                break;
            }
            case "10":
            {
                await app.ReceivePersonUsingDirectBinderSubscriber();
                break;
            }
            case "11":
            {
                await app.ExecuteConsumer();
                break;
            }
            case "12":
            {
                await app.ExecuteSubscriberBound01();
                break;
            }
            case "13":
            {
                await app.MultipleSubscriberBound01();
                break;
            }
            case "14":
            {
                await app.ExecuteBoundConsumer();
                break;
            }
            case "15":
            {
                await app.ExecuteTempSubscriber();
                break;
            }
            default:
            {
                Console.WriteLine("No option available !!!");
                break;
            }
        }
    }

    private async Task ExecuteDirectSubscriber()
    {
        var descriptor = new SubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            PrefetchCount = 10,
            //Queue = { Name = "demo" }
            Queue = { Name = "demo", AutoDelete = false, Durable = true }
            //Queue = { Name = "demo", AutoDelete = false, Durable = true, Arguments =
            //{
            //    {"x-queue-type", "quorum"}
            //}}
            //Queue = { Name = "demo", AutoDelete = false, Durable = true, Arguments =
            //{
            //    {"x-queue-type", "stream"},
            //    //{"x-max-age", "10m"},
            //    {"x-stream-max-segment-size-bytes", 1000_000}
            //}},
            //Arguments =
            //{
            //    {"x-stream-offset", "first"}
            //}

            //first, last, next
        };

        var subscriber = new CommonSubscriber<string>(descriptor, this.dataFormatter);
            
        this.OnSubscribing(subscriber);

        await Task.FromResult(0);
    }

    private async Task ExecuteConsumer()
    {
        var descriptor = new SubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            PrefetchCount = 10,
            Queue = { Name = "demo", AutoDelete = false, Durable = true, Arguments =
            {
                {"x-queue-type", "quorum"}
            } }
        };

        using (var consumer = new BasicConsumer<string>(descriptor, this.dataFormatter))
        {
            consumer.OnException += this.SubscriberOnException;
            consumer.Received += this.SubscriberOnReceived;
            consumer.Received += this.SubscriberOnReceived2;

            Console.WriteLine("Consumer setup completed !!!");

            await consumer.Execute();

            Console.WriteLine(" Consumer ends correctly..");
        }

    }

    private async Task ExecuteBoundConsumer()
    {
        var name = Guid.NewGuid().ToString("D");

        var publisherDesc = new PublisherBoundDescriptor
        {
            EnableChannelEvents = true,
            Confirmation =
            {
                Enabled = true
            },
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = name,
                    Type = ExchangeTypes.Direct,
                    Durable = false,
                    AutoDelete = true,
                    Queues =
                    {
                        new QueueBoundDescriptor{ Name = name, AutoDelete = true, Durable = false }
                    }
                }
            }
        };

        var consumerDesc = new SubscriberBoundDescriptor
        {
            PrefetchCount = 5,
            Queue = { Name = name, AutoDelete = true, Durable = false }
        };

        using (var connection = this.connectionFactory.BuildConnection(factory => factory.CreateConnection("demo-pub")))
        {
            using (var channel = connection.CreateModel())
            {
                // publisher
                var publisher = new PublisherBound(publisherDesc, this.dataFormatter, channel);
                publisher.OnChannelEventFired
                    .Add(async (_, args) =>
                    {
                        Console.WriteLine($"### date: {DateTime.UtcNow:u}, event: {args.EventTag}");
                        await Task.Delay(TimeSpan.FromSeconds(5));
                        Console.WriteLine($"### date: {DateTime.UtcNow:u}, handler is over ###");
                    })
                    .Add(async (_, args) =>
                    {
                        Console.WriteLine($">>> date: {DateTime.UtcNow:u}, event: {args.EventTag}");
                        await Task.Delay(TimeSpan.FromSeconds(5));
                        Console.WriteLine($">>> date: {DateTime.UtcNow:u}, handler is over >>>");
                    })
                    ;

                for (var i = 0; i < 10; i++)
                {
                    await publisher.SendAsync($"message nr: {i}");
                }

                Console.WriteLine(">>> all messages were published !!!");

                Console.WriteLine(">>> waiting for all confirms.");

                if (channel.WaitForConfirms())
                {
                    Console.WriteLine(">>> all messages were confirmed.");
                }

                Console.WriteLine($"### {DateTime.UtcNow:u}, Consumer setup started !!!");

                // consumer
                var consumer = new ConsumerBound<string>(consumerDesc, this.dataFormatter, channel);
                consumer.OnException.Add(this.SubscriberOnException);
                consumer.Received.Add(this.SubscriberOnReceivedAsync1);
                consumer.Received.Add(this.SubscriberOnReceivedAsync1);

                Console.WriteLine($"### {DateTime.UtcNow:u}, Consumer setup completed !!!");

                await consumer.Execute();

                Console.WriteLine($"### {DateTime.UtcNow:u}, Consumer ends correctly..");
            }
        }
    }

    private async Task ExecuteDirectSubscriberFake()
    {
        var descriptor = new SubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            PrefetchCount = 50,
            Queue = { Name = "demo" }
        };

        var subscriber = new CommonSubscriber<string>(descriptor, this.dataFormatter);

        Console.WriteLine($"##### Starting subscriber, apikey: {this.apiKey} #####");

        using (subscriber)
        {
            subscriber.OnException += this.SubscriberOnException;
            subscriber.Received += this.SubscriberOnReceivedFake;
                
            //subscriber.Received += SubscriberOnReceived2;
                
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        await Task.FromResult(0);
    }

    private async Task ExecuteDirectSubscriberRequeue()
    {
        var descriptor = new SubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            PrefetchCount = 1,
            Queue = { Name = "demo", AutoDelete = false, Durable = true, Arguments = { { "x-queue-type", "quorum" } }},
            RetryPolicy = { Enabled = true, AutoDelete = false, Durable = true, MaxRetry = 3, Delayed = TimeSpan.FromSeconds(20) }
        };

        var subscriber = new CommonSubscriber<string>(descriptor, this.dataFormatter);

        Console.WriteLine($"##### Starting subscriber, apikey: {this.apiKey} #####");

        using (subscriber)
        {
            subscriber.OnMaxRetryFailed += this.SubscriberOnOnMaxRetryFailed;
            subscriber.OnException += this.SubscriberOnException;
            subscriber.Received += this.SubscriberOnReceivedFake;
                
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        await Task.FromResult(0);
    }

    private async Task ExecuteBinderSubscriberRequeue()
    {
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            PrefetchCount = 1,
            Exchange = { Name = "demo-topic", Type = ExchangeTypes.Topic, Durable = true, AutoDelete = false },
            Queue = { Name = "demo", AutoDelete = false, Durable = true, Arguments = { { "x-queue-type", "quorum" } } },
            RetryPolicy = { Enabled = true, AutoDelete = false, Durable = true, MaxRetry = 3, Delayed = TimeSpan.FromSeconds(20) }
        };

        var subscriber = new BinderSubscriber<string>(descriptor, this.dataFormatter);

        Console.WriteLine($"##### Starting subscriber, apikey: {this.apiKey} #####");

        using (subscriber)
        {
            subscriber.OnMaxRetryFailed += this.SubscriberOnOnMaxRetryFailed;
            subscriber.OnException += this.SubscriberOnException;
            subscriber.Received += this.SubscriberOnReceivedFake;

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        await Task.FromResult(0);
    }

    private async Task ExecuteDirectBinderSubscriber()
    {
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            PrefetchCount = 50,
            Queue = { Name = "demo", Durable = true, AutoDelete = false, Arguments = { {"x-queue-type", "quorum"} }},
            Exchange = { Name = "demo-direct", Type = ExchangeTypes.Direct }
        };

        var subscriber = new BinderSubscriber<string>(descriptor, this.dataFormatter);

        this.OnSubscribing(subscriber);

        await Task.FromResult(0);
    }

    private async Task ReceivePersonUsingDirectBinderSubscriber()
    {
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Queue = { Name = "person" },
            Exchange = { Name = "person-direct", Type = ExchangeTypes.Direct }
        };

        using (var subscriber = new BinderSubscriber<IEnumerable<Person>>(descriptor, this.dataFormatter))
        {
            Console.WriteLine($"##### Starting subscriber, apikey: {this.apiKey} #####");

            subscriber.OnException += this.SubscriberOnException;

            subscriber.Received += (_, args) =>
            {
                Console.WriteLine($"Message \t: {args.Message.Data}, created: {args.Message.Created}");
                args.Acknowledged = true;
            };

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        await Task.FromResult(0);
    }

    private async Task ExecuteDirectBinderSubscriberWithRouting(string routingKey, string queue)
    {
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Queue = { Name = queue },
            Exchange = { Name = "demo-direct-route", Type = ExchangeTypes.Direct },
            Binder = { RoutingKey = routingKey }
        };

        var subscriber = new BinderSubscriber<string>(descriptor, this.dataFormatter);

        this.OnSubscribing(subscriber);

        await Task.FromResult(0);
    }

    private async Task ExecuteFanoutBinderSubscriber(string queue)
    {
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Queue = { Name = queue },
            Exchange = { Name = "demo-broadcast", Type = ExchangeTypes.Fanout }
        };

        var subscriber = new BinderSubscriber<string>(descriptor, this.dataFormatter);

        this.OnSubscribing(subscriber);

        await Task.FromResult(0);
    }

    private async Task ExecuteTopicBinderSubscriber(string routeContract, string queue)
    {
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Queue = { Name = queue },
            Exchange = { Name = "demo-topic-route", Type = ExchangeTypes.Topic },
            Binder = { RoutingKey = routeContract }
        };

        var subscriber = new BinderSubscriber<string>(descriptor, this.dataFormatter);

        this.OnSubscribing(subscriber);

        await Task.FromResult(0);
    }

    private async Task ExecuteDirectSubscriberWithHttpRequest()
    {
        var descriptor = new SubscriberDescriptor
        {
            Address = this.AppSettings.EsbAddress,
            Credentials = this.AppSettings.EsbCredentials,
            Queue = { Name = "demo" }
        };
            
        Console.WriteLine($"##### Starting subscriber, apikey: {this.apiKey} #####");

        using (var subscriber = new CommonSubscriber<HttpRestRequest>(descriptor, this.dataFormatter))
        {
            subscriber.Received += (_, args) =>
            {
                var mex = args.Message;
                var ser = $"id: {mex.Id}, created: {mex.Created}, route: {mex.Route}";
                Console.WriteLine(ser);

                var payload = this.dataFormatter.Decode<DemoCls>(mex.Data.Content);
                Console.WriteLine($"payload: method: {mex.Data.Method}, uri: {mex.Data.Resource}, version: {mex.Data.Version}, Data: [id: {payload.Id}, value: {payload.Value}, price: {payload.Price}]");
                Console.WriteLine();
            };

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        await Task.FromResult(0);
    }

    private async Task ExecuteSubscriberBound01()
    {
        var descriptor = new SubscriberBoundDescriptor
        {
            Queue =
            {
                Name = "demo_01_q", Durable = true, AutoDelete = false, Arguments =
                {
                    {"x-queue-type", "quorum"}
                }
            }
        };

        using (var connection =
               this.connectionFactory.BuildConnection(factory => factory.CreateConnection("demo-sub")))
        {
            using (var channel = connection.CreateModel())
            {
                var subscriber = new SubscriberBound<string>(descriptor, this.dataFormatter, channel);
                this.OnSubscribingWithStartStop(subscriber);
            }
        }

        await Task.FromResult(0);
    }

    private async Task MultipleSubscriberBound01()
    {
        using (var connection =
               this.connectionFactory.BuildConnection(factory => factory.CreateConnection("demo-sub")))
        {
            using (var channel = connection.CreateModel())
            {
                var tsk1 = Task.Factory.StartNew(async () =>
                {
                    var descriptor = new SubscriberBoundDescriptor
                    {
                        Queue =
                        {
                            Name = "demo_01_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "quorum"}
                            }
                        }
                    };

                    // ReSharper disable once AccessToDisposedClosure
                    var subscriber = new SubscriberBound<string>(descriptor, this.dataFormatter, channel);
                    await this.OnSubscribing(subscriber, "001");
                });

                var tsk2 = Task.Factory.StartNew(async () =>
                {
                    var descriptor = new SubscriberBoundDescriptor
                    {
                        Queue =
                        {
                            Name = "demo_02_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "classic"}
                            }
                        }
                    };

                    // ReSharper disable once AccessToDisposedClosure
                    var subscriber = new SubscriberBound<string>(descriptor, this.dataFormatter, channel);
                    await this.OnSubscribing(subscriber, "002");
                });

                Task.WaitAll(tsk1, tsk2);
            }
        }

        await Task.FromResult(0);
    }

    private async Task ExecuteTempSubscriber()
    {
        var queueName = Guid.NewGuid().ToString();
        Console.WriteLine($"queue name: {queueName}");

        var descriptor = new SubscriberBoundDescriptor
        {
            Queue =
            {
                Name = queueName, Exclusive = true
            },
            Exchange = new ExchangeBoundDescriptor
            {
                Name = "temp", Durable = true, Type = ExchangeTypes.Direct
            },
            Binder = new BinderDescriptor
            {
                RoutingKey = queueName
            }
        };

        using (var connection = this.asyncConnectionFactory.CreateConnection("sub-4-temp"))
        {
            using (var channel = connection.CreateModel())
            {
                var subscriber = new SubscriberBound<string>(descriptor, this.dataFormatter, channel);

                await this.OnSubscribing(subscriber, this.apiKey);
            }
        }

        await Task.FromResult(0);
    }

    private async Task OnSubscribing(IAsyncSubscriber<string> subscriber, string id)
    {
        var rnd = new Random();

        Console.WriteLine($"##### Starting subscriber, id: {id} #####");

        subscriber.OnException.Add(this.SubscriberOnException);
        subscriber.Received.Add(
            (_, args) =>
            {
                Console.WriteLine($"id: {id}, message: {args.Message.Data}, created: {args.Message.Created}");
                args.Acknowledged = true;

                //var delay = rnd.Next(5, 7);
                //Thread.Sleep(TimeSpan.FromSeconds(delay));
            });

        Console.WriteLine(" Press [enter] to exit.");

        if (subscriber is IAsyncRunnable runnable)
        {
            await runnable.Start();
        }

        Console.ReadLine();
    }

    private void OnSubscribingWithStartStop(IAsyncSubscriber<string> subscriber)
    {
        Console.WriteLine($"##### Starting subscriber, apikey: {this.apiKey} #####");

        subscriber.Received.Add(this.SubscriberOnReceived);
        subscriber.Received.Add(this.SubscriberOnReceived2);
        subscriber.OnException.Add(this.SubscriberOnException);

        if (subscriber is IAsyncRunnable runnable)
        {
            runnable.Start();
            Console.WriteLine(runnable.IsRunning);

            runnable.Start();
            Console.WriteLine(runnable.IsRunning);

            runnable.Start();
            Console.WriteLine(runnable.IsRunning);

            Console.WriteLine("stopping now ...........");

            runnable.Stop();
            Console.WriteLine(runnable.IsRunning);

            runnable.Stop();
            Console.WriteLine(runnable.IsRunning);

            runnable.Stop();
            Console.WriteLine(runnable.IsRunning);

            //// first run
            //var status = runnable.Start();
            //Console.WriteLine($"status: {status}, subscriber has started, sleeping for 1 minutes...");
            //Thread.Sleep(TimeSpan.FromMinutes(1));

            //status = runnable.Stop();
            //Console.WriteLine($"status: {status}, subscriber stops for 1 minute...");
            //Thread.Sleep(TimeSpan.FromMinutes(1));

            //// second run
            //status = runnable.Start();
            //Console.WriteLine($"status: {status}, subscriber has started, sleeping for 1 minutes...");
            //Thread.Sleep(TimeSpan.FromMinutes(1));

            //Console.WriteLine("stop subscriber... press [enter] to exit.");
            //runnable.Stop();
        }

        Console.ReadLine();
    }

    private void OnSubscribing(ISubscriber<string> subscriber)
    {
        Console.WriteLine($"##### Starting subscriber, apikey: {this.apiKey} #####");

        subscriber.Received += this.SubscriberOnReceived;
        subscriber.Received += this.SubscriberOnReceived2;
        subscriber.OnException += this.SubscriberOnException;

        Console.ReadLine();
    }

    private void SubscriberOnReceived(object sender, MessageDeliverEventArgs<string> args)
    {
        Console.WriteLine($"handler v1, message: {args.Message.Data}, created: {args.Message.Created}");
        args.Acknowledged = true;

        Thread.Sleep(TimeSpan.FromSeconds(1));
    }

    private void SubscriberOnReceived2(object sender, MessageDeliverEventArgs<string> args)
    {
        Console.WriteLine($"handler v2, message: {args.Message.Data}");
    }

    private async Task SubscriberOnReceivedAsync1(object sender, MessageDeliverEventArgs<string> args)
    {
        Console.WriteLine($"handler v1, message: {args.Message.Data}, arrived: {DateTime.UtcNow:u}");
        args.Acknowledged = true;

        await Task.Delay(TimeSpan.FromSeconds(1));
    }

    private async Task SubscriberOnReceivedAsync2(object sender, MessageDeliverEventArgs<string> args)
    {
        Console.WriteLine($"handler v2, message: {args.Message.Data}, arrived: {DateTime.UtcNow:u}");
        args.Acknowledged = true;

        await Task.Delay(TimeSpan.FromSeconds(1));
    }

    private void SubscriberOnReceivedFake(object sender, MessageDeliverEventArgs<string> args)
    {
        Console.WriteLine($"Message \t: {args.Message.Data}, created: {args.Message.Created}, now {DateTime.UtcNow:u}");
        args.Acknowledged = false;
        args.Requeue = true;

        Thread.Sleep(TimeSpan.FromSeconds(1));
    }

    private void SubscriberOnException(object sender, MessageExceptionEventArgs e)
    {
        Console.WriteLine($"An exception was caught, payloadType: {e.PayloadType}, exception: [message: {e.Exception.Message}, stackTrace: {e.Exception.StackTrace}]");
    }

    private void SubscriberOnOnMaxRetryFailed(object sender, RetryExceptionEventArgs<string> e)
    {
        Console.WriteLine($"An exception was caught for retry, payload: {e.Message}, exception: [message: {e.Exception.Message}, stackTrace: {e.Exception.StackTrace}]");
    }
}
